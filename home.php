<?php

/**
 * @file
 * The PHP page that serves all page requests on a Drupal installation.
 *
 * The routines here dispatch control to the appropriate handler, which then
 * prints the appropriate page.
 *
 * All Drupal code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 */

/**
 * Root directory of Drupal installation.
 */
require __DIR__ . '/sites/default/settings.php';

ob_start();
$lang = 'en';
if(isset($_SERVER['HTTP_REFERER'])) {
  $path_str =  parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH);

  $path = explode('/', $path_str);

  if (isset($path[2]) ) {
    if ($path[2] == 'ar') {
      $lang = 'ar';
    }
    if($path[1] == 'ae') {
      $userCountry = 'AE';
    }
  }
  elseif(isset($path[1])) {
   if($path[1] == 'ae') {
     $userCountry = 'AE';
   } 
  }
}

if (!$userCountry) {
  if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != '') {
    $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
  }
  else {
    $ip_address = $_SERVER['REMOTE_ADDR'];
  }
  $userCountry = get_user_country($ip_address);
  
  if (!in_array(strtoupper($userCountry), array('AE', 'QA', 'KE', 'TZ', 'KZ', 'BH', 'KW', 'SA', 'YE', 'LY', 'EG', 'OM', 'LB', 'PK', 'IQ', 'TH'))) {
    $userCountry = 'AE';
  }
}


if (isset($userCountry)) {
  $base_url = "http://blog.shoexpressme.com.php7-32.phx1-2.websitetestlink.com";
  $url = $base_url . '/' . strtolower($userCountry) . '/'. $lang;
  header("Location: $url");
  ob_end_flush();
  exit();
}

function get_user_country($ip) {
  global $databases;
  $username = $databases['default']['default']['username'];
  $password = $databases['default']['default']['password'];
  $host = $databases['default']['default']['host'];
  $ip_num=sprintf("%u",ip2long($ip));
  $con = mysqli_connect($host, $username, $password);
  $db_selected = mysqli_select_db( $con, $databases['default']['default']['database']);
  if (!$db_selected) {
    die ("Can\'t use test_db : " . mysqli_error($con));
  }

  $query = "SELECT country_code FROM ip2c WHERE '" . mysqli_real_escape_string($con, $ip_num) . "' BETWEEN begin_ip_num AND end_ip_num";
  $result = mysqli_query($con, $query);
  if(!$result) {
    die("Database query failed: " . mysqli_error($con));
  }
  while($res = mysqli_fetch_array($result)) {
    $countryIp = strtolower($res['country_code']);
  }
  return $countryIp;
}