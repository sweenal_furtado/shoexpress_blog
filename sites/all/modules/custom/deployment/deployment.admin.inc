<?php

function deployment_content_list () {
	
  if (isset($_GET['node_type']) && ! empty($_GET['node_type'])) {
    $list_form = drupal_get_form('deployment_content_table_list');
    $output = render($list_form);
  }
  else
  {
    $filter_form = drupal_get_form('deployment_select_content');
	  $output = render($filter_form);
      
  }    

	return $output;
}
function _custom_node_type_get_names(){
  $node = node_type_get_names();
  $NodeValue = array();  
  foreach($node as $key => $type)
  {
    $query = db_select('deployment_uuid','d');
    $query->fields('d',array('deployment_status'));
    $query->condition('d.type',$key);
    $query->condition('d.pushed',0);
    $query->condition('d.deployment_status',array('insert_to_live','update_to_live'),'IN');
    $query->addExpression('count(nid)');
    $query->groupBy('d.deployment_status');
    $record = $query->execute()->fetchAllKeyed(0,1);
    
    if(isset($record) && !empty($record))
    {
      $CounterString = '';
      foreach($record as $ikey=>$val)
      {
        if($ikey == 'insert_to_live')
        {
          $CounterString.= ' Insert to Live ('.$val.')';
        }
        elseif($ikey == 'update_to_live')
        {
          $CounterString.= ' Update to Live ('.$val.')';
        }
      }
      $sap = '';
      if(isset($CounterString) && !empty($CounterString))
      {
        $sap =" : ";
      }
      $NodeValue[$key] = $type.$sap.$CounterString;
    }
  }
  return $NodeValue;
  
}
function deployment_select_content ($form, &$form_state) {
  $NodeValue = _custom_node_type_get_names();
  if(isset($NodeValue) && !empty($NodeValue))
  {  
    $form['#method'] = 'get';
    $form['node_type'] = array(
  		'#title' => t('Select type'),
      '#type' => 'radios',
  		'#options' => _custom_node_type_get_names(),
  		'#description' => t('Select content type you want to deploy'),
  		'#required' => TRUE,
  		'#default_value' => isset($_GET['node_type']) ? $_GET['node_type'] : '',
  		'#empty_option' => t('- All -'),
  	);
    $form['deployment_type'] = array(
      '#type' => 'hidden', 
      '#value' => 'all_to_live', 
    );
  	$form['submit'] = array(
  		'#type' => 'submit',
  		'#value' => t('Submit'),
  		'#suffix' => '<a href="' . url('admin/deployment') . '">Reset</a>',
  	);
  }
  else
  {
    $form['noresult'] = array(
      '#type'=>'item',
      '#markup' => t('Everything is sync, there is no more content found to be pushed on live.'),
    );
  }
	return $form;
}

function deployment_content_table_list ($form, &$form_state) {
	//kpr($_POST);
	$header = array(
		'nid' => 'Nid',
		'title' => t('Title'),
		'deployment_status' => t('Deployment Status'),
		'changed' => t('Updated time')
	);
	$options = array();
	if (isset($_GET['deployment_type']) && ! empty($_GET['deployment_type'])) {
		$query = db_select('deployment_uuid', 'du');//->extend('PagerDefault');
		$query->leftJoin('node', 'n', 'du.nid = n.nid');
		$query->addTag('node_access');
		$query->fields('du', array('nid', 'changed', 'deployment_status'));
		$query->fields('n', array('title'));
    if(isset($_GET['node_type']) && !empty($_GET['node_type']))
    {
		  $query->condition('du.type', $_GET['node_type']);
    }
    if($_GET['deployment_type'] == 'all_to_live')
    {
      $query->condition('du.deployment_status', array('insert_to_live','update_to_live'),'IN');
    }
    else
    {              
      $query->condition('du.deployment_status', $_GET['deployment_type']);        
    }    
    $query->condition('du.pushed',0);
		#$query->limit(10)
		      $query->orderBy('changed', 'DESC');
		$result = $query->execute()
		                ->fetchAllAssoc('nid');
										
										#dpr($result);
		foreach ($result as $item) {
			$options[$item->nid] = array(
				'nid' => $item->nid,
				'title' => $item->title,
				'deployment_status' => $item->deployment_status,
				'changed' => format_date($item->changed, 'short'),
			);
		}
		$form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Push To Live'),
			'#weight' => 3,
      '#suffix' => '<a href="' . url('admin/deployment') . '">Cancel</a>',
		);
	}
	$form['table'] = array(
		'#type' => 'tableselect',
		'#header' => $header,
		'#options' => $options,
		'#empty' => t('No content available.'),
		'#weight' => 1
	);
	$form['pager'] = array(
		'#markup' => theme('pager'),
		'#weight' => 2
	);

	return $form;
}

function deployment_content_table_list_validate (&$form, &$form_state) {
	//kpr($form_state['values']); exit();
	$nids = array_filter($form_state['values']['table']);
	if (empty($nids)) {
		form_set_error('table', t('Please select atleast one content to be pushed'));
	}
}

function deployment_content_table_list_submit (&$form, &$form_state) {
	global $base_url;
	$nids = array_filter($form_state['values']['table']);
	if (! empty($nids)) {
	 	$nodes = node_load_multiple($nids);
    if (! empty($nodes)) {
      /*foreach($nodes as $node)
      {
        if($node->nid != $node->tnid)
        {
          $tnode = node_load($node->tnid);
          array_push($nodes,$tnode);
        }
      }*/
      $response['deployment_type'] = $_GET['deployment_type'];
      $response['node_type'] = $_GET['node_type'];      
			$response['nodes'] = $nodes;
      $response['beta_url'] = $base_url;
			$url = variable_get('deployment_url');
			if(!empty($url))
      {
        $options = array(
  				'data' => json_encode($response),
  				'method' => 'POST',
  				'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
  			);
  			$data_response = drupal_http_request($url, $options);
        if ($data_response->code == 200) {
  				$data = drupal_json_decode($data_response->data);
  				if (isset($data['error']) && $data['error'] == "true") {
  				  if(isset($data['message'])) {
  				    drupal_set_message(t('Something wrong happened, hint : :hint', array(':hint'=>$data['message'])),'error');
            } else {
              drupal_set_message(t('Something wrong happened'),'error');
            }
          }
  				else {
  				  if (isset($data['uuids']) && ! empty($data['uuids'])) {
  						db_update('deployment_uuid')
  						->fields(array('pushed' => 1))
  						->condition('uuid', array_keys($data['uuids']), 'IN')
  						->execute();
  						drupal_set_message(t('Content successfully pushed, Want to push other <a href="' . url('admin/deployment') . '">contents</a>?'));
  					}
  				}
  			}
      }
      else
      {
        form_set_error('table', t('Please set the deployement url <a href="@url">here</a>',array('@url'=>$base_url.'/admin/deployment/seturl')));
      }
		}
	}
}

/**
 * Callback for setting the deployment url.
 */ 
function deployment_set_url($form,$form_state) {
  $form['deployment_url'] = array(
    '#type' => 'textfield',
    '#title' => 'Deployment URL',
    '#default_value' => variable_get('deployment_url'),
    '#require' => true,              
  );
  $form['deployment_revert_url'] = array(
    '#type' => 'textfield',
    '#title' => 'Deployment Revert URL',
    '#default_value' => variable_get('deployment_revert_url'),
    '#require' => true,              
  );
  return system_settings_form($form);    
}

/**
 * Callback for Revert content form
 */ 
function deployment_revert_content_list () {
  $filter_form = drupal_get_form('deployment_select_revert_content');
  $output = render($filter_form);
  $list_form = drupal_get_form('deployment_revert_content_table_list');
  $output .= render($list_form);
  return $output;
}

function deployment_select_revert_content ($form, &$form_state) {
	$form['#method'] = 'get';
	$form['node_type'] = array(
		'#title' => t('Select type'),
		'#type' => 'select',
		'#options' => node_type_get_names(),
		'#description' => t('Select content type you want to revert'),
		'#required' => TRUE,
		'#default_value' => isset($_GET['node_type']) ? $_GET['node_type'] : '',
		'#empty_option' => t('-Select-'),
	);
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Submit'),
		'#suffix' => '<a href="' . url('admin/deployment/revert') . '">Reset</a>',
	);

	return $form;
}

function deployment_revert_content_table_list ($form, &$form_state) {
	//kpr($_POST);
	$header = array(
		'nid' => 'Nid',
		'title' => t('Title'),
		'deployment_status' => t('Deployment Status'),
		'changed' => t('Updated time')
	);
	$options = array();
	if (isset($_GET['node_type']) && ! empty($_GET['node_type'])) {
		$query = db_select('deployment_uuid', 'du');//->extend('PagerDefault');
		$query->leftJoin('node', 'n', 'du.nid = n.nid');
		$query->addTag('node_access');
		$query->fields('du', array('nid', 'changed', 'deployment_status'));
		$query->fields('n', array('title'));
    $query->condition('du.deployment_status', 'update_to_live');
		$query->condition('du.type', $_GET['node_type']);
		$query->condition('du.pushed',1);
		//$query->limit(10)
		$query->orderBy('changed', 'DESC');
		$result = $query->execute()
		                ->fetchAllAssoc('nid');
		foreach ($result as $item) {
			$options[$item->nid] = array(
				'nid' => $item->nid,
				'title' => $item->title,
				'deployment_status' => $item->deployment_status,
				'changed' => format_date($item->changed, 'short'),
			);
		}
		$form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Revert'),
			'#weight' => 3,
		);
	}
	$form['table'] = array(
		'#type' => 'tableselect',
		'#header' => $header,
		'#options' => $options,
		'#empty' => t('No content available.'),
		'#weight' => 1
	);
	$form['pager'] = array(
		'#markup' => theme('pager'),
		'#weight' => 2
	);

	return $form;
}
function deployment_revert_content_table_list_validate (&$form, &$form_state) {
	$nids = array_filter($form_state['values']['table']);
	if (empty($nids)) {
		form_set_error('table', t('Please select atleast one content to be reverted'));
	}
}
function deployment_revert_content_table_list_submit($form,&$form_state) {
  global $base_url;
	$nids = array_filter($form_state['values']['table']);
	if (! empty($nids)) {
		$nodes = node_load_multiple($nids);
    if (! empty($nodes)) {
      /*foreach($nodes as $node)
      {
        if($node->nid != $node->tnid)
        {
          $tnode = node_load($node->tnid);
          array_push($nodes,$tnode);
        }
      }*/
      $response['nodes'] = $nodes;
      $response['beta_url'] = $base_url;
			$url = variable_get('deployment_revert_url');
      if(!empty($url))
      {
        $options = array(
  				'data' => json_encode($response),
  				'method' => 'POST',
  				'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
  			);
  			$data_response = drupal_http_request($url, $options);
        if ($data_response->code == 200) {
  				$data = drupal_json_decode($data_response->data);
  				if (isset($data['error']) && $data['error'] == "true") {
  					drupal_set_message(t('Something wrong happened, hint @hint', array('@hint'=>$data['message'])));
  				}
  				else {
  				  if (isset($data['uuids']) && ! empty($data['uuids'])) {
  						db_update('deployment_uuid')
  						->fields(array('pushed' => 1)) 
  						->condition('uuid', array_keys($data['uuids']), 'IN')
  						->execute();
              foreach($nodes as $rnode)
              {
                $nid = $rnode->nid;
                
                $revision_list = node_revision_list($rnode);
                if(isset($revision_list) && !empty($revision_list))
                {
                  if(count($revision_list) > 1)
                  {
                    $lastRevision = array_slice($revision_list, 1, 1, true);
                  }
                  else
                  {
                    $lastRevision = $revision_list;
                  }
                  if(isset($lastRevision) && !empty($lastRevision))
                  {
                    $vid = array_shift(array_keys($lastRevision));
                    $node_rev = node_load($nid, $vid, $reset = FALSE);
                    if(isset($node_rev) && !empty($node_rev))
                    {              
                      node_save($node_rev);
                      node_revision_delete($rnode->vid);
                    }
                  }
                }
                else
                {
                  $data['error'] = true;
                }
              }
              if (isset($data['error']) && $data['error'] == "true") 
              {
                drupal_set_message(t('Content revert fail'),'error');
              }
              else
              {
  						  drupal_set_message(t('Content successfully reverted'));
              }
  					}
  				}
  			}
      }
      else
      {
        form_set_error('table', t('Please set the deployement revert url <a href="@url">here</a>',array('@url'=>$base_url.'/admin/deployment/seturl')));
      }
		}
	}
}