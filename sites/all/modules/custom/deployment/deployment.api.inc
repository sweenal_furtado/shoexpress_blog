<?php
define('UUID_PATTERN', '[0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}');
/**
 * @file
 * Api related features
 */
function deployment_node_save_api () {
	$data = file_get_contents('php://input');
	$response = array();
	$uuids = array();
	if (isset($data)) {
		try {
			$request = drupal_json_decode($data);
			if (isset($request['nodes']) && !empty($request['nodes'])) {
				if (isset($request['deployment_type']) && ($request['deployment_type'] == 'update_to_live' || $request['deployment_type'] == 'insert_to_live' || $request['deployment_type'] == 'all_to_live')) {
				  foreach ($request['nodes'] as $node) {
						$live_nid = deployment_get_nid_uuid($node['uuid']);
						$live_node = (object)$node;
						if ($live_nid) {
							$live_node->nid = $live_nid;
              $lnode = node_load($live_nid);
              $live_node->vid = $lnode->vid;
						}
						else {
							$live_node->nid = NULL;
              $live_node->vid = NULL;
						}
						$live_node->tnid = NULL;
            $live_node->revision = 1;
						$live_node->uid = 1;

						$instances = field_info_instances('node', $node['type']);

						foreach ($instances as $k => $instance) {
							$field = field_info_field($instance['field_name']);
							#print_r($field);
							$field_language = array_shift(array_keys($node[$k]));
							if (!empty($node[$k][$field_language])) {
								
								if ($field['module'] == 'image') {

									foreach ($node[$k][$field_language] as $delta => $field_image) {
										$uri = str_replace('public://', '', $field_image['uri']);
										$url = $request['beta_url'] . '/' . variable_get('file_public_path', 'sites/default/files') . '/' . rawurlencode($uri);
										$image = file_get_contents($url);
										$file = file_save_data($image, $field_image['uri'], FILE_EXISTS_RENAME);

										if ($file) {
											$live_node->{$k}[$field_language][$delta] = (array)$file;
										}
									}
								}
								else if ($field['module'] == 'text') {
									foreach ($node[$k][$field_language] as $delta => $field_body) {
										$image_tag_find = array_shift($field_body);
									
										// read all image tags into an array
										preg_match_all('/<img[^>]+>/i',$image_tag_find, $imgTags); 
										
										for ($i = 0; $i < count($imgTags[0]); $i++) {
										 // get the source string
											preg_match('/src="([^"]+)/i',$imgTags[0][$i], $imgage);
											// remove opening 'src=' tag, can`t get the regex right
											$origImageSrc = str_ireplace( 'src="', '',  $imgage[0]);
											
											$origImagearr = explode('/',$origImageSrc);
											$image_name = array_pop((array_slice($origImagearr, -1)));
											
											$url = $request['beta_url'] . '/' . variable_get('file_public_path', 'sites/default/files') . '/' . $image_name;
		
										  #echo "<pre>".print_r($url,"\n")."</pre>";exit;
											$image = file_get_contents($url);
									
											$file = file_save_data($image, 'public://' . '/' . rawurldecode($image_name), FILE_EXISTS_RENAME);

										}
									
									}
								}
								else if ($field['module'] == 'taxonomy') {
                  if(isset($node[$k][$field_language]) && !empty($node[$k][$field_language])) {
                    $checkVocabulary = taxonomy_vocabulary_machine_name_load($node[$k][$field_language][0]['vocabulary_machine_name']);
                    if(isset($checkVocabulary) && !empty($checkVocabulary)) {
    									foreach ($node[$k][$field_language] as $term) {
                        $terms = taxonomy_get_term_by_name($term['name'], $term['vocabulary_machine_name']);
                        if(!isset($terms) || empty($terms)) {
                          $newterm->vid = $checkVocabulary->vid;
                          $newterm->name = $term['name'];
                          $newterm->description = $term['description'];
                          $newterm->format = $term['format'];
                          $newterm->weight = $term['weight'];
                          $newterm->vocabulary_machine_name = $term['vocabulary_machine_name'];
                          $terms = taxonomy_term_save($newterm);
                        }
    									}
                    } else {

                    }
                  }
                  unset($live_node->{$k}[$field_language]); // Removed the previous data
									$term_array = json_decode(json_encode($terms), true);
                  $live_node->{$k}[$field_language][] = array_shift($term_array);
								}
							}
						}
            node_save($live_node);
						if ($node->tnid != 0) {
							if ($node->tnid == $node->nid) {
								db_update('node')
								->fields(array('tnid' => $live_node->nid))
								->condition('nid', $live_node->nid)
								->execute();
							}
							else {
								$beta_tnid = isset($nodes[$node->tnid]) ? $nodes[$node->tnid] : '';
								if ($beta_tnid) {
									$tnid_uuid = $beta_tnid['uuid'];
									$tnid = deployment_get_nid_uuid($tnid_uuid);
									db_update('node')
									->fields(array('tnid' => $tnid))
									->condition('nid', $live_node->nid)
									->execute();
								}
							}
						}
						$uuids[$live_node->uuid] = $live_node->nid;
					}
          $response['error'] = "false";
	        $response['uuids'] = $uuids;
				}
				else if (isset($request['deployment_type']) && $request['deployment_type'] == 'delete_to_live') {
					if (isset($response['nodes']) && !empty($response['nodes'])) {
						$nodes = drupal_json_decode($response['nodes']);
						foreach ($nodes as $node) {
							$uuids[] = $node['uuid'];
						}

						if ($uuids) {
							$live_nids = db_select('deployment_uuid', 'du')
							             ->fields('du', array('nid', 'uuid'))
							             ->condition('uuid', $uuids, 'IN')
							             ->execute()
							             ->fetchAllKeyed(0, 1);
							node_delete_multiple($live_nids);
							$response['error'] = "true";
							$uuids = array_keys($nodes);
						}
					}
				}
				else {
					$response['error'] = "true";
				}
			}
			else {
				$response['error'] = "true";
			}

		}
		catch (Exception $e) {
			$response['error'] = "true";
			$response['message'] = $e->getMessage();
		}
	}
	else {
		$response['error'] = "true";
	}


	return $response;
}
/**
 * Api Callback for Node Revert
 */
function deployment_node_revert_api() {
  $data = file_get_contents('php://input');
	$response = array();
  	if (isset($data)) {
		try {
			$request = drupal_json_decode($data);
      if (isset($request['nodes']) && !empty($request['nodes'])) {
				foreach ($request['nodes'] as $node) {
		      if($node['deployment_status'] == 'update_to_live' && $node['pushed'] == 1)
          {
            $nid = deployment_get_nid_uuid($node['uuid']);
            if ($nid) {
  						$lnode = node_load($nid);
              $revision_list = node_revision_list($lnode);
              if(isset($revision_list) && !empty($revision_list))
              {
                if(count($revision_list) > 1)
                {
                  $lastRevision = array_slice($revision_list, 1, 1, true);
                }
                else
                {
                  $lastRevision = $revision_list;
                }
                if(isset($lastRevision) && !empty($lastRevision))
                {
                  $vid = array_shift(array_keys($lastRevision));
                  $node_rev = node_load($nid, $vid, $reset = FALSE);
                  if(isset($node_rev) && !empty($node_rev))
                  {
                    node_save($node_rev);
                    node_revision_delete($lnode->vid);
                  }
                  else
                  {
                    $response['error'] = "true";
                  }
                }
              }
              else
              {
                $response['error'] = "true";
              }
  					}
  					$uuids[$node['uuid']] = $nid;
          }
				}
        if($response['error'] != 'true')
        {
          $response['error'] = "false";
          $response['uuids'] = $uuids;
        }
			}
			else {
				$response['error'] = "true";
			}
		}
		catch (Exception $e) {
			$response['error'] = "true";
			$response['message'] = $e->getMessage();
		}
	}
	else {
		$response['error'] = "true";
	}
	return $response;
}
/**
 * Finds nid from uuid
 * @param $uuid
 * universal unique identfier
 * @return $nid
 * node id
 */
function deployment_get_nid_uuid ($uuid) {
	$query = db_select('deployment_uuid', 'du')
	         ->fields('du', array('nid'))
	         ->condition('uuid', $uuid);
	$result = $query->execute()
	                ->fetchField();

	return $result;
}

/**
 * Generates an universally unique identifier.
 *
 * This function first checks for the PECL alternative for generating
 * universally unique identifiers. If that doesn't exist, then it falls back on
 * PHP for generating that.
 *
 * @return
 *   An UUID, made up of 32 hex digits and 4 hyphens.
 */
function deployment_uuid_generate () {
	$callback = drupal_static(__FUNCTION__);

	if (empty($callback)) {
		if (function_exists('uuid_create') && !function_exists('uuid_make')) {
			$callback = '_deployment_uuid_generate_pecl';
		}
		elseif (function_exists('com_create_guid')) {
			$callback = '_deployment_uuid_generate_com';
		}
		else {
			$callback = '_deployment_uuid_generate_php';
		}
	}

	return $callback();
}

/**
 * Generates a UUID using the Windows internal GUID generator.
 *
 * @see http://php.net/com_create_guid
 */
function _deployment_uuid_generate_com () {
	// Remove {} wrapper and make lower case to keep result consistent.
	return drupal_strtolower(trim(com_create_guid(), '{}'));
}

/**
 * Generates an universally unique identifier using the PECL extension.
 */
function _deployment_uuid_generate_pecl () {
	return uuid_create(UUID_TYPE_DEFAULT);
}

/**
 * Generates a UUID v4 using PHP code.
 *
 * Based on code from http://php.net/uniqid#65879, but corrected.
 */
function _deployment_uuid_generate_php () {
	// The field names refer to RFC 4122 section 4.1.2.
	return sprintf('%04x%04x-%04x-4%03x-%04x-%04x%04x%04x', // 32 bits for "time_low".
		mt_rand(0, 65535), mt_rand(0, 65535), // 16 bits for "time_mid".
		mt_rand(0, 65535), // 12 bits after the 0100 of (version) 4 for "time_hi_and_version".
		mt_rand(0, 4095), bindec(substr_replace(sprintf('%016b', mt_rand(0, 65535)), '10', 0, 2)), // 8 bits, the last two of which (positions 6 and 7) are 01, for "clk_seq_hi_res"
		// (hence, the 2nd hex digit after the 3rd hyphen can only be 1, 5, 9 or d)
		// 8 bits for "clk_seq_low" 48 bits for "node".
		mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}

function deployment_system_retrieve_file ($url, $destination = NULL, $managed = FALSE, $replace = FILE_EXISTS_RENAME) {	$parsed_url = parse_url($url);
	if (!isset($destination)) {
		$path = file_build_uri(drupal_basename($parsed_url['path']));
	}
	else {
		if (is_dir(drupal_realpath($destination))) {
			// Prevent URIs with triple slashes when glueing parts together.
			$path = str_replace('///', '//', "$destination/") . drupal_basename($parsed_url['path']);
		}
		else {
			$path = $destination;
		}
	}
	$result = drupal_http_request($url);
	if ($result->code != 200) {
		drupal_set_message(t('HTTP error @errorcode occurred when trying to fetch @remote.', array('@errorcode' => $result->code,
			'@remote' => $url)), 'error');

		return FALSE;
	}
	if ($managed) {
		$file = file_save_data($result->data, $path, $replace);
	}
	else {
		$file = file_unmanaged_save_data($result->data, $path, $replace);
	}
	if (!$file) {
		drupal_set_message(t('@remote could not be saved to @path.', array('@remote' => $url, '@path' => $path)), 'error');
	}

	return $file;
}
