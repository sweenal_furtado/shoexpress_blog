<?php
 /**
  * Database Configuration Form
  */
function admin_betatolive_configuration($form,&$form_state)
{
	//$values = $form_state['values'];
	global $skip_tables;
	$form['skip_tables']=array(
	'#type'=>'textfield',
	'#title'=>t('Table Name'),
	'#description' => t('Enter the name of the table which you want to skip in push to live operation'), 
	'#default_value' =>implode(',',variable_get('skip_tables',array())), 
	'#size' => 60, 
	'#maxlength' => 300, 
	);
	$form['skip_tables_info'] = array(
	'#type' => 'fieldset', 
	'#title' => t('Table that are available for beta to live operation'), 
	'#weight' => 3, 
	'#collapsible' => TRUE, 
	'#collapsed' => true, 
	);

	$form['skip_tables_info']['table_name']=array(
	'#type'=>'item',
	'#markup'=>implode('<br>',array_diff(betatolive_get_database_tables(),array_merge($skip_tables,variable_get('skip_tables',array())))), 
	);
	$form['skip_directory']=array(
	'#type'=>'textfield',
	'#title'=>t('Directory Name'),
	'#description' => t('Enter the name of the directory which you want to exclude in push to live operation'), 
	'#default_value' =>implode(',',variable_get('skip_directory',array())), 
	'#size' => 60, 
	'#maxlength' => 128, 
	);
	$form['actions']['submit'] = array(
	'#type' => 'submit',
	'#value' => t('Save configuration'),
	);
	$form['#theme'] = 'system_settings_form';
	$form['global_skip_tables']=array(
	'#type'=>'item',
	'#markup'=>'<span>List of tables which are common for all the concept <i>'.implode(', ',$skip_tables).'</i></span>',
	);
	return $form;
}

/**
* Database Configuration Validate 
*/
function admin_betatolive_configuration_validate($form,&$form_state)
{
	// Validation
	global $skip_tables;
	$error = false;
	$values = $form_state['values'];
	if(trim($values['skip_tables'])=='') return;
	$raw_skip_tables = explode(',',trim($values['skip_tables']));
	foreach($raw_skip_tables as $raw_skip_table)
	{
		$table=trim($raw_skip_table);
		if(!db_table_exists($table) || in_array($table,$skip_tables))
		{
			$error=true;
			$invalid_table[]=$table;
		}
	}
	if($error)
		form_set_error('skip_tables', t('Table :table is not a valid table name or it might be included in the commonly skip table list',array(':table'=>implode(',',$invalid_table))));
}

/**
* Database Configuration Save 
*/
function admin_betatolive_configuration_submit($form,&$form_state)
{
$values = $form_state['values'];
$raw_skip_tables = explode(',',$values['skip_tables']);
foreach($raw_skip_tables as $raw_skip_table)
	{
		$table=trim($raw_skip_table);
		(!empty($table))?$skip_tables[$table]=$table:'';
	}
	variable_set('skip_tables',$skip_tables);

$raw_dirs = explode(',',$values['skip_directory']);
foreach($raw_dirs as $raw_dir)
	{
		$dir=trim($raw_dir);
		(!empty($dir))?$skip_directory[$dir]=$dir:'';
	}
	variable_set('skip_directory',$skip_directory);
	drupal_set_message(t('Configuration options has been saved'));
}

/**
* Return available tables on instance
*/
function betatolive_get_database_tables()
{
	/*
	$database_schema = &drupal_static(__FUNCTION__,array(),true);
	if(isset($database_schema))
	{
		return $database_schema;
	}
	*/
	$database_schema = db_query('SHOW TABLES')->fetchAllKeyed(0,0);
	
	return $database_schema;
}
/**
* Form to define the instance
*/
function admin_betatolive_instance_configuration($form,&$form_state)
{
	global $_instance;
	$inital_path='/mnt/stor5-wc2-dfw1/495544/';
	if($_instance=='www')
	{
		$form['instance']=array(
		'#type'=>'item',
		'#markup'=>t('Cannot configured the module on live instance'),
		);
		return $form;
	}
	$form['instance']=array(
	'#type'=>'item',
	'#markup'=>t('<b> Beta to live configured on: </b>'. $_instance .' enviornment'),
	);
	$form['beta_path']=array(
	'#type'=>'item',
	'#markup'=>t('<b> Beta Backup Path: </b>'.str_replace($inital_path,'',BETA_DEPLOYMENT_PATH)),
	);
	$form['live_path']=array(
	'#type'=>'item',
	'#markup'=>t('<b> Live Backup Path: </b>'.str_replace($inital_path,'',LIVE_DEPLOYMENT_PATH)),
	);
	$form['live_url']=array(
	'#type'=>'item',
	'#markup'=>t('<b> Publish Content on : </b/>'.LIVE_URL),
	);

	return $form;
}
/**
* Validate handler
*/
function admin_betatolive_instance_configuration_validate($form,&$form_state)
{

}
/**
* Submit handler
*/
function admin_betatolive_instance_configuration_submit($form,&$form_state)
{
	$instance = $form_state['values']['instance'];
	variable_set('betatolive_instance',$instance);
	drupal_set_message(t('Configuration options has been saved'));
}