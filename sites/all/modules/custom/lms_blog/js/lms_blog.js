;(function($) {
	$(document).ready(function() {

		$('.field-name-field-content-for-the-blog #edit-field-content-for-the-blog-en input[type="radio"]').on('change', function(e) {
			
            var checkValue = $(this).val();           
            if(checkValue == 0){
            	$('.form-type-checkbox.form-item-status input[type="checkbox"]').prop("checked", false);
            }
			else{
                 $('.form-type-checkbox.form-item-status input[type="checkbox"]').prop("checked", true);
			}
			
		});  

	});
})(jQuery);
  