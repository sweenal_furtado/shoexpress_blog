<?php
$form = drupal_get_form('lms_blog_form');
$categories = drupal_render($form['blog_categories']);
$years = drupal_render($form['blog_year']);
$months = drupal_render($form['blog_month']);
?>

<div class="row sort-line">
	<div>
		<?php print($categories);?>
	</div>
	<div>
		<?php print($years);?>
	</div>
	<div class="line-height">
		<?php print($months);?>
	</div>
</div>

