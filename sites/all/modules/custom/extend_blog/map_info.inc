<?php
 function _map_info(){
  drupal_add_css(drupal_get_path('module','extend_blog').'/css/attractions.css');
  drupal_add_css(drupal_get_path('module','extend_blog').'/css/map-styles.css');
  drupal_add_js(drupal_get_path('module','extend_blog').'/js/jquery.superbox.js');
  drupal_add_js(drupal_get_path('module','extend_blog').'/js/jquery-1.4.2.min.js', 'file', array('weight'=> 1));
  drupal_add_js(drupal_get_path('module','extend_blog').'/js/attractions.js', array('weight'=> 1));
  drupal_add_js(drupal_get_path('module','extend_blog').'/js/autoload.js');
  drupal_add_js(drupal_get_path('module','extend_blog').'/js/slimbox2.js');
  drupal_add_js('http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=AIzaSyBFng5kBnjR5scHoxViOReIUfLZVcuemY0');
	global $base_url,$theme;
	$output = '';
 $output.=' 
            <div class="Container container_12 clearfix">
                <div class="grid_9 alpha" style="width:100%;"> 
                     <h1 style="font-size: 2em;line-height: 1;margin-bottom: 0.75em;font-weight: bold;">In &amp; Around</h1>

		    <ul id="hotel_list"> 

                        <li class="hotel_list activee"><a href="#albarsha">Citymax Al Barsha</a></li> 

                        <li class="hotel_list"><a href="#burdubai">Citymax Bur Dubai</a></li>

                        <li class="hotel_list"><a href="#sharjah">Citymax Sharjah</a></li>

                    </ul>
                    <div id="map_slider_container"> 
                        <div id="divMapContainer" style="padding: 3px;" > 
                            <div id="map_div"> 
                                <div class="map" id="map_canvas"></div> 
                                <div id="filter_div" style="margin:0 auto;"> 
                                    <span id="attractspan"><a href="#" id="selectall"> Select all </a> 
                                        <a href="#" id="showfilter" class="showr"> Show Filters</a><a href="#" id="hidefilter" class="hider"> Hide Filters</a></span> 
                                    <ul id="categories"> 
                                        <li><input type="checkbox" class="check" id="visit"/><label for="visit">&nbsp;<img src="images/icon_16/visit_icon.png" class="category_img" alt="visit"/>&nbsp;Place to visit</label></li> 
                                        <li><input type="checkbox" class="check" id="food"/><label for="food">&nbsp;<img src="images/icon_16/food_icon.png" class="category_img" alt="food"/>&nbsp;Food & Dining</label></li> 
                                        <li><input type="checkbox" class="check" id="shopping"/><label for="shopping">&nbsp;<img src="images/icon_16/shopping_icon.png" class="category_img" alt="shopping"/>&nbsp;Shopping</label></li>	
                                        <li><input type="checkbox" class="check" id="business"/><label for="business">&nbsp;<img src="images/icon_16/business_icon.png" class="category_img" alt="business"/>&nbsp;Business Area</label></li>         <li><input type="checkbox" class="check" id="transport"/><label for="transport">&nbsp;<img src="images/icon_16/transport_icon.png" class="category_img" alt="transport"/>&nbsp;Transportaion</label></li> 
                                    </ul> 
                                </div> 
                            </div> 
                            <div id="slider_div"> 
                                <div id="image_list"> 
                                    <div id="slideshow"> 
                                        <span id="sliderspan"></span> 
                                    </div> 
                                    <div class="divPosRel" style="position:relative" > 
                                        <a class="dir" href=""><img id="prev1" src="images/arrow-prev.png" alt="prev"/></a>
                                        <a class="dir" href=""><img id="next1" src="images/arrow-next.png" alt="next"/> </a>
                                    </div> 
                                </div> 
                            </div> 
                            <div id="divListing"> 
                                <div id="listdiv"> 
                                    <ul id="map_list"></ul> 
                                </div> 
                            </div> 
                            <div id="map_message"></div> 
                            <div class="divPosRel" style="position:relative;" > 
                                <a href="" class="dir"><img id="prev" src="images/arrowprev.png" alt="prev" /> </a>
                                <a href="" class="dir"><img id="next" src="images/arrownext.png" alt="next"/> </a>
                            </div>			
                        </div> 
                    </div>	
                </div>                                                           
            </div>	
        
';
return $output;
 }