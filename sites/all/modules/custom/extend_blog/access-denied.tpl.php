<?php
global $base_url,$sitesdomain,$server_name;
?>

<!-- ### Inner Page Content ### -->

				<div id="notfound">
					<h2>Sorry...</h2>
					<h3>Someone broke the page</h3>
					<p>You could visit the <a href="<?php print $base_url.'/blog' ?>">homepage</a> or try the following:</p>
					<ul>
						<li>Double-check your URL if you manually typed it in.</li>
						<li>Let us know at webmaster [at] <?php print strtolower('babyshopstores') ?> [dot] com <br />if you reached this page via a link from another page on this site.</li>
					</ul>
				</div>

<!-- ### END Inner Page Content ### -->
