;(function($) {
	$(document).ready(function() {
		$('.news-holder ul li').eq(0).addClass('selected');
		$('.news-preview div').eq(0).addClass('top-content');
		$('.loadmoreclick').click(function() {
			var ID = $(this).attr("id");
			var new_ID = ID.split('_');
			if(new_ID[0] && new_ID[1] == 0){
				$('.loadmoreclick').hide();
				$(".loadingimage").show();
				$.ajax({
					type: "POST",
					url: Drupal.settings.basePath+"blog/ajax",
					data: { created: new_ID[0] },
					cache: false,
					success: function(html){
						if(html == 'nodata'){
								$('.loadingimage').empty();
								$('.loadingimage').append('No more entries found');
						}else{
							var new_html = html.split('_CityMAxBlog_');
							var new_new_html = new_html[1].split('_');
							if(new_html[0] != 'nodata') {
								//console.log(new_html[0]);
								$(".grid-list--thrids").append(new_html[0]);
								$(".loadmore").appendTo('.grid-8');
								$(".grid-list--thrids .clearfloat").appendTo('.grid-8');
								$('.loadmoreclick').attr('id', new_new_html[0]+'_'+new_ID[1]);
								if(new_new_html[1] == '1') {
									$('.loadmore').remove();
									$('.loadingimage').empty();
									$('.loadingimage').append('No more entries found');
								}else {
								    $(".loadingimage").hide();
								    $('.loadmoreclick').show();
								    $('.loadtext').show();
								}
							}else {
								//alert('empty');
								$('.loadingimage').empty();
								$('.loadingimage').append('No more entries found');
							}							
						}

					}
				});
			}else if(new_ID[0] && new_ID[1] != 0) {
				$('.loadmoreclick').hide();
				$(".loadingimage").show();
				$.ajax({
					type: "POST",
					url: Drupal.settings.basePath+"blog/ajax",
					data: { created: new_ID[0], tid: new_ID[1] },
					cache: false,
					success: function(html){
						if(html == 'nodata'){
								$('.loadingimage').empty();
								$('.loadingimage').append('No more entries found');
						}else{
							var new_html = html.split('_CityMAxBlog_');
							var new_new_html = new_html[1].split('_');
							if(new_html[0] != 'nodata') {
								
								$(".grid-list--thrids").append(new_html[0]);
								$(".loadmore").appendTo('.grid-8');
								$(".grid-list--thrids .clearfloat").appendTo('.grid-8');
								$('.loadmoreclick').attr('id', new_new_html[0]+'_'+new_ID[1]);
								if(new_new_html[1] == '1') {
								    $(".loadingimage").hide();
								    $('.loadmoreclick').show();
								    $('.loadtext').show();
								}else {
								    $('.loadmore').remove();
								}
							}
							else {
								$('.loadingimage').empty();
								$('.loadingimage').append('No more entries found');
							}
						}
					}
				});
			}
			else
			{
				$('.loadingimage').empty();
				$('.loadingimage').append('No more entries found');
			}
			return false;
		});
		var Blog={initAll:function(){this.sidebarToggles()},sidebarToggles:function(){$(".js-sidebar-list .trigger").live("click",function(e){$(this).parent().toggleClass("closed");$(this).toggleClass("opened");e.preventDefault()})}};Blog.initAll();
		/*
		var disqusPublicKey = "u4b6bXWJOBteryncU6x2k2haxp2L2bAThz0aZv3I5rAoDUQ2NNBB3kS9Dr64nflM";
		var disqusShortname = "citymax"; // Replace with your own shortname

		var urlArray = [];
		$('.count-comments').each(function () {
		  var url = $(this).attr('data-disqus-url');
		  urlArray.push('link:' + url);
		});
		
		  $.ajax({
			type: 'GET',
			url: "https://disqus.com/api/3.0/threads/set.jsonp",
			data: { api_key: disqusPublicKey, forum : disqusShortname, thread : urlArray },
			cache: false,
			dataType: 'jsonp',
			success: function (result) {

			  for (var i in result.response) {

				var countText = " comments";
				var count = result.response[i].posts;

				if (count == 1)
				  countText = " comment";

				$('div[data-disqus-url="' + result.response[i].link + '"]').html('<h4>' + count + countText + '</h4>');

			  }
			}
		  }); */
	});
})(jQuery);
