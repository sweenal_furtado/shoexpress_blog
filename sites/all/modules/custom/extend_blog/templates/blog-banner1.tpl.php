<?php //dpr($nodes);exit;   ?>
<ul class="sliders-holder">
	<?php foreach ($nodes as $node) {
		//dpr($node);
		$img = file_create_url($node->field_blog_banner_image[LANGUAGE_NONE][0]['uri']);
		//dpr($img);exit;
		$alt = $node->field_blog_banner_image[LANGUAGE_NONE][0]['title'];
		$link = $node->field_blog_banner_link[LANGUAGE_NONE][0]['value'];
	?>
	<li>
		<a href="<?php print url($link, array('absolute' => TRUE)); ?>" class="full">
			<div class="content">
				<div class="info-section">
					<?php echo $node->body[LANGUAGE_NONE][0]['value'];?>
				</div>
			</div>
			<img src="<?php print $img; ?>" alt="<?php print $alt; ?>" />
		</a>
	</li>
	<?php } ?>
</ul>
<div class="switcher-holder blue">
	<ul>
	<?php 
	$i=0;
	foreach ($nodes as $node) {?> 
		<li><a href="#"><?php echo $i;?></a></li>
	<?php $i++;} ?>	
	</ul>
</div>

