;(function($) {
	$(document).ready(function() {
		$('.loadmoreclick').click(function() 
		{
			var ID = $(this).attr("id");
			var new_ID = ID.split('_');
			if(new_ID[0] && new_ID[1] == 0)
			{
                $('.loadmoreclick').hide();
				$(".loadingimage").show();
				$.ajax({
					type: "POST",
					url: Drupal.settings.basePath+"blog/ajax",
					data: { created: new_ID[0] },
					cache: false,
					success: function(html){
						var new_html = html.split('_H@M#C#NTRE_');
                        var new_new_html = new_html[1].split('_');
						if(new_html[0] != 'nodata') {
							$(".entries").append(new_html[0]);
							$(".loadmore").appendTo('.entries');
							$(".entries .clearfloat").appendTo('.entries');
							$('.loadmoreclick').attr('id', new_new_html[0]+'_'+new_ID[1]);
                            if(new_new_html[1] == '1') {
    							$(".loadingimage").hide();
                                $('.loadmoreclick').show();
    							$('.loadtext').show();
                            }
                            else {
                                $('.loadmore').remove();
                            }
						}
						else {
							$('.loadingimage').empty();
							$('.loadingimage').append('No more entries found');
						}
					}
				});
			}
			else if(new_ID[0] && new_ID[1] != 0)
			{
                $('.loadmoreclick').hide();
				$(".loadingimage").show();
				$.ajax({
					type: "POST",
					url: Drupal.settings.basePath+"blog/ajax",
					data: { created: new_ID[0], tid: new_ID[1] },
					cache: false,
					success: function(html){
						var new_html = html.split('_H@M#C#NTRE_');
                        var new_new_html = new_html[1].split('_');
						if(new_html[0] != 'nodata') {
							$(".entries").append(new_html[0]);
							$(".loadmore").appendTo('.entries');
							$(".entries .clearfloat").appendTo('.entries');
							$('.loadmoreclick').attr('id', new_new_html[0]+'_'+new_ID[1]);
                            if(new_new_html[1] == '1') {
    							$(".loadingimage").hide();
                                $('.loadmoreclick').show();
    							$('.loadtext').show();
                            }
                            else {
                                $('.loadmore').remove();
                            }
						}
						else {
							$('.loadingimage').empty();
							$('.loadingimage').append('No more entries found');
						}
					}
				});
			}
			else
			{
				$('.loadingimage').empty();
				$('.loadingimage').append('No more entries found');
			}
			return false;
		});
	});
})(jQuery);