<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */

hide($content['comments']);
hide($content['links']);
global $base_url, $language;
$lang = $language->language;

$path = explode('/',drupal_get_path_alias(current_path()));
?>
<?php if ($page) { ?>
  <a href="<?php print $base_url . '/' . $lang . '/' . $path[0];?>" class="back-to" id="back-to1"><?php echo t('Back to blog'); ?></a>
  <?php } ?>
  <div id="blog-item-<?php print $node->nid; ?>" class="item <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    <h2 id="blog-item-<?php print $node->nid; ?>-title"><?php print $title; ?></h2>
    <ul class="share list-inline" id="product-share">
      <li id="product-share-item1">
        <a target="_blank" href="https://www.facebook.com/sharer.php?u=<?php echo url('node/'.$node->nid, array('absolute'=>true));?>&amp;title=<?php echo urlencode($node->title);?>" class="p-button-social has-icon facebook js-button-social " data-analytics-social="facebook" >
          <span class="p-button-social__label"><?php echo t('Like'); ?></span>
          <span class="p-button__count" data-facebook-share-count-url="<?php echo url('node/'.$node->nid, array('absolute'=>true));?>" data-use-parens="true"></span>
        </a>
      </li>
      <li id="product-share-item2">
        <a target="_blank" href="https://twitter.com/intent/tweet?counturl=<?php echo url('node/'.$node->nid, array('absolute'=>true));?>&amp;text=<?php echo urlencode($node->title);?> &amp;url=<?php echo url('node/'.$node->nid, array('absolute'=>true));?>&amp;via=Babyshop" class="p-button-social has-icon twitter js-button-social " data-analytics-social="twitter">
          <span class="p-button-social__label"><?php echo t('Tweet'); ?></span>
          <span class="p-button__count" data-twitter-share-count-url="<?php echo url('node/'.$node->nid, array('absolute'=>true));?>" data-use-parens="true"></span>
        </a>
      </li>
    </ul>
    <time id="blog-item-<?php print $node->nid; ?>-time" datetime="<?php print $submitted_datetime;?>"><?php print $submitted; ?></time>
    <?php if(isset($node->field_image[$lang][0]['alt']) && !$page) { ?>
      <div class="img-holder img-responsive" id="img-holder-01">
        <?php
        $alt = isset($node->field_image[$lang][0]['alt'])?$node->field_image[$lang][0]['alt']: '';
        if (empty($alt)) {
          $alt = '';
        }
        $uri = isset($node->field_image[$lang][0]['uri'])?file_create_url($node->field_image[$lang][0]['uri']): '';
        ?>
        <img id="blog-item-<?php print $node->nid; ?>-img" src="<?php print $uri; ?>" alt="<?php print $alt;?>">
      </div>
    <?php } ?>
    <div id="blog-item-<?php print $node->nid; ?>-text"><?php print render($content); ?></div>
    <?php if($teaser) { ?>
      <?php $id = isset($_GET['archive_id']) ? $_GET['archive_id'] : $node->view->args[0] ;?>
      <a id="blog-item-<?php print $node->nid; ?>-more" href="<?php print $node_url.'?archive_id='.$id; ?>"><?php echo t('Continue reading'); ?></a>
    <?php } ?>
  </div>
