
<?php
/**
* @file
* Default theme implementation to display a single Drupal page.
*
* The doctype, html, head and body tags are not in this template. Instead they
* can be found in the html.tpl.php template in this directory.
*
* Available variables:
*
* General utility variables:
* - $base_path: The base URL path of the Drupal installation. At the very
*   least, this will always default to /.
* - $directory: The directory the template is located in, e.g. modules/system
*   or themes/bartik.
* - $is_front: TRUE if the current page is the front page.
* - $logged_in: TRUE if the user is registered and signed in.
* - $is_admin: TRUE if the user has permission to access administration pages.
*
* Site identity:
* - $front_page: The URL of the front page. Use this instead of $base_path,
*   when linking to the front page. This includes the language domain or
*   prefix.
* - $logo: The path to the logo image, as defined in theme configuration.
* - $site_name: The name of the site, empty when display has been disabled
*   in theme settings.
* - $site_slogan: The slogan of the site, empty when display has been disabled
*   in theme settings.
*
* Navigation:
* - $main_menu (array): An array containing the Main menu links for the
*   site, if they have been configured.
* - $secondary_menu (array): An array containing the Secondary menu links for
*   the site, if they have been configured.
* - $breadcrumb: The breadcrumb trail for the current page.
*
* Page content (in order of occurrence in the default page.tpl.php):
* - $title_prefix (array): An array containing additional output populated by
*   modules, intended to be displayed in front of the main title tag that
*   appears in the template.
* - $title: The page title, for use in the actual HTML content.
* - $title_suffix (array): An array containing additional output populated by
*   modules, intended to be displayed after the main title tag that appears in
*   the template.
* - $messages: HTML for status and error messages. Should be displayed
*   prominently.
* - $tabs (array): Tabs linking to any sub-pages beneath the current page
*   (e.g., the view and edit tabs when displaying a node).
* - $action_links (array): Actions local to the page, such as 'Add menu' on the
*   menu administration interface.
* - $feed_icons: A string of all feed icons for the current page.
* - $node: The node object, if there is an automatically-loaded node
*   associated with the page, and the node ID is the second argument
*   in the page's path (e.g. node/12345 and node/12345/revisions, but not
*   comment/reply/12345).
*
* Regions:
* - $page['help']: Dynamic help text, mostly for admin pages.
* - $page['highlighted']: Items for the highlighted content region.
* - $page['content']: The main content of the current page.
* - $page['sidebar_first']: Items for the first sidebar.
* - $page['sidebar_second']: Items for the second sidebar.
* - $page['header']: Items for the header region.
* - $page['footer']: Items for the footer region.
*
* @see template_preprocess()
* @see template_preprocess_page()
* @see template_process()
* @see html.tpl.php
*
* @ingroup themeable
*/

global $theme_path,$base_url;
$theme_full_path = $base_url . '/' . $theme_path;
$all_concepts = array();
$other_concepts = array();

$name = 'concept';
$myvoc = taxonomy_vocabulary_machine_name_load($name);
$tree = taxonomy_get_tree($myvoc->vid);
$brand = $brand_id = '';

foreach ($tree as $term) {
	if($term->tid == 13) {
		continue;
	}
  if($term->depth == 0) {
    $term_detail = taxonomy_term_load($term->tid);
    $term_alias = drupal_get_path_alias('taxonomy/term/' . $term->tid);
    $all_concepts[$term_alias]['tid'] = $term_detail->tid;
    $all_concepts[$term_alias]['name'] = $term_alias;
    $all_concepts[$term_alias]['logo'] = file_create_url(isset($term_detail->field_image['und'][0]['uri'])?$term_detail->field_image['und'][0]['uri'] : '');
  }
}

$path = explode('/',current_path());

if(empty($path[2]) && $path[0] == 'node' && is_numeric($path[1]) || isset($path[1]) && $path[1] == 'archive' ) {
  $current_path = explode('/',drupal_get_path_alias(current_path()));
  $brand = $current_path[0];
  $brand_id = $all_concepts[$brand]['tid'];
}
else if($path[0] == 'taxonomy' && $path[1] =='term' && is_numeric($path[2])) {
  $parents =  taxonomy_get_parents($path[2]);
  if(empty($parents)) {
    $brand = isset($path[2]) ? drupal_get_path_alias($path[2]) : $all_concepts[current_path()];
    $brand_id = isset($path[2]) ? $path[2] : $all_concepts[current_path()]['tid'];
  }
  else {
    $current_path = explode('/',drupal_get_path_alias(current_path()));
    $brand = $current_path[0];
    $brand_id = isset($all_concepts[$brand]['tid']) ? $all_concepts[$brand]['tid'] : '';
  }
}

$brand_term = taxonomy_term_load($brand_id);
$brand_logo = file_create_url(isset($brand_term->field_image['und'][0]['uri'])?$brand_term->field_image['und'][0]['uri']:'');

foreach($all_concepts as $key => $value) {
  if($key != $brand) {
    $other_concepts[$key] = $value;
  }
}

// $main_domain = '';
// $pos = strpos($_SERVER['HTTP_HOST'], 'uat');
// if ($pos === false) {
//   $main_domain = 'http://www.babyshopstores.com/';
// }
// else {
//   $main_domain = 'http://uat1.babyshopstores.com/';
// }
?>

<script type="text/javascript">
    var LMS = window.LMS || {},
    LMSdataLayer = window.LMSdataLayer || [{
      dimension2: 'Guest',
      dimension3: '',
      dimension4: navigator.userAgent.substr(0, 100)
    }],
    StrandsTrack = window.StrandsTrack || [];

    LMS.pageData = LMS.pageData || {};
    LMS.pageData.localizedData = LMS.pageData.localizedData || {};

    LMS.pageData.gtm = '';

    window.LMS = LMS;
    window.LMSdataLayer = LMSdataLayer;
    window.StrandsTrack = StrandsTrack;

    LMS.pageData.zopimUrl = '//v2.zopim.com/?25BhNvbz0DoGTL4bHzjKAcnfBxNKyMiP';
  </script>

<!-- main container of all the page elements -->
<div id="wrapper">
  <?php if ($messages): ?>
    <?php print $messages; ?>
  <?php endif; ?>
  <!-- header of the page -->
  <?php 
  global $current_country;
  // print $current_country;exit();
  $file_name = 'header_' .strtolower($current_country) . '_' .'en' . '.html'; 
  require_once DRUPAL_ROOT . '/sites/default/files/header_footer/'.$file_name; 
  ?>

<!-- add navigation of the page -->
<!-- <?php
$path = explode('/',drupal_get_path_alias(current_path()));
if(trim($path[0]) == 'lifestyle' || trim($path[0]) == 'max' || trim($path[0]) == 'splash' || trim($path[0]) == 'babyshop' || trim($path[0]) == 'homecentre' || trim($path[0]) == 'shoemart') {
  $brand_name = ucfirst($path[0]);
}
else {
  $path[0] = '';
  $brand_name = 'babyshop';
}
?> -->
<!-- <?php if(isset($brand_nav)) { ?>
<nav class="brand-menu" id="add-navigation">
  <?php print($brand_nav) ;?>
</nav>
<?php } ?> -->


<!-- contain main informative part of the site -->
<main role="main" id="main-part">
  <!-- address book section -->
  <div class="brands-section" id="brands-section">
    <div class="container-fluid" id="brands-section-container">   
        <div class="" id="blogs-list">
          <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
          <?php print render($page['content']); ?>

        </div>
      </div>
    </div>
  </main>
  <!-- footer of the page -->
  <?php
   $file_name = 'footer_' . strtolower($current_country) . '_' . 'en' . '.html';
   require_once DRUPAL_ROOT . '/sites/default/files/header_footer/'.$file_name;
  ?>
</div>
</div>

