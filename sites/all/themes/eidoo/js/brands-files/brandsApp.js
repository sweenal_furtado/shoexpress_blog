define(['vendor/googleMap', 'vendor/gallery'],
	function(initMap, initCarousel) {
		initCarousel(); // slide gallery
		initMap(); // slide gallery
	}
);
