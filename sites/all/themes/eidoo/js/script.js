;(function($) {
  $(document).ready(function() {
    var APP = {
      init: function() {
        this.customSelect();
      },
      customSelect: function() {
        createDropDown();
        $(".dropdown dt a").on('click', function(e) {
          e.preventDefault();
          var dropID = $(this).closest("dl").attr("id");
          $("#" + dropID).find("ul").toggleClass('offscreen');
        });
        $(document).on('click', function(e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").addClass('offscreen');
        });
        $(".dropdown dd ul a").on('click', function(e) {
          e.preventDefault();
          var dl = $(this).closest("dl");
          var dropID = dl.attr("id");
          var text = $(this).html();
          var source = dl.prev();
          $("#" + dropID + " dt a").html(text).attr('class', $(this).attr('class'));
          $("#" + dropID + " dd ul").addClass('offscreen');
          source.val($(this).find("span.value").html());
        });

        function createDropDown() {
          var selects = $("select.custom-dropdown");
          var idCounter = 1;
          selects.each(function() {
            var dropID = "dropdown-" + $(this).attr('id');
            var source = $(this);
            var selected = source.find("option[selected]");
            var classValue = selected.val().slice(-2).toLowerCase();
            var options = $("option", source);
            var imgsPath = Drupal.settings.basePath + "sites/all/themes/eidoo/images/flags/"; 
            source.after('<dl id="' + dropID + '" class="dropdown"></dl>');
            if (dropID === "dropdown-country") {
              $("#" + dropID).append('<dt><a href="#" class="' + classValue + '"><img class="flag" src="' + imgsPath + classValue + '.png">' + selected.text() + '<span class="value">' + selected.val() + '</span></a></dt>');
              $("#" + dropID).append('<dd><ul class="offscreen"></ul></dd>');
              options.each(function() {
                var thiz = $(this),
                  value = thiz.val(),
                  text = thiz.text(),
                  countryClass = value.slice(-2).toLowerCase();
                $("#" + dropID + " dd ul").append('<li><a href="#" class=' + countryClass + '><img class="flag" src="' + imgsPath + countryClass + '.png">' + Drupal.t(text) + '<span class="value">' + value + '</span></a></li>');
              });
            } else {
              $("#" + dropID).append('<dt><a href="#" class="' + classValue + '">' + selected.text() + '<span class="value">' + selected.val() + '</span></a></dt>');
              $("#" + dropID).append('<dd><ul class="offscreen"></ul></dd>');
              options.each(function() {
                var thiz = $(this),
                  value = thiz.val(),
                  text = thiz.text(),
                  itemClass = text.toLowerCase();
                $("#" + dropID + " dd ul").append('<li><a href="#" class=' + itemClass + '>' + text + '<span class="value">' + value + '</span></a></li>');
              });
            }
            idCounter++;
          });
        }
      }
    };
    APP.init();
        //Country Switcher
   $('#dropdown-country > dd > ul > li > a').click(function() {
      var country = $(this).children('span').html().toLowerCase();
      var url = $(location).attr('href');
      redirectUrl = url.replace(Drupal.settings.basePath, "/" + country + "/");
      location.href = redirectUrl;
		});
  });
   $("#newsLetterForm").validate({
     
      submitHandler: function(form) {   
        console.log('Testing');
        var email= $('#newsLetterForm input[type="email"]').val();        
        var base_path = window.location.protocol + "//" + window.location.host + "/";  
         console.log(email);  
        $.ajax({ 
          //url: base_path + Drupal.settings.basePath + '/subscribe/'+email,
          url: base_path +'subscribe/'+email, 
          data: {'email':email},
          success: function(data) {
              alert('success');
            if(data == "subscribed_new" || data == "update_subscription") {
              $('#newsLetterForm input').val('');
              //$("#newsLetterForm label").closest("label").text("Thanks for signing up to our newsletter.");
              $("#newsLetterForm").prepend("<label>Thanks for signing up to our newsletter.</label>");
              $(".parsley-error-text").hide();
            }
            else if(data == "alreadysubscribed") {
              $('#newsLetterForm input').val('');
             // $("#newsLetterForm label").closest("label").text("You have already subscribed.");
              $("#newsLetterForm").prepend("<label>You have already subscribed.</label>");
              $(".parsley-error-text").hide();
              return false;
            }
            else if(data == "fail") { 
              $('#newsLetterForm input').val('');
              //$("#newsLetterForm label").closest("label").text("Please check your email address.");
              $("#newsLetterForm").prepend("<label>Please check your email address.</label>");
              $(".parsley-error-text").hide();
              return false;
            }
            else 
            {  
              return false;
            }
          }
        });
      }
    });
})(jQuery);
