(function($) {
  $(document).ready(function(){
    jcf.replaceAll();
    var country_lang = Drupal.settings.site.language;
    var country = Drupal.settings.site.country;
    $('.node-blog-ecommerce .field-name-body .field-items img').removeAttr('style');
    $('.node-blog-ecommerce .field-name-body .field-items img').addClass('img-responsive');

    $('#categories-field').appendTo('.form-item-blog-categories');
    $('#archives-field').appendTo('.form-item-blog-year');

    $(".form-item-blog-categories select#categories-field").wrap("<div class='select-holder'></div>");
    $('.form-item-blog-categories span.jcf-select').appendTo('.form-item-blog-categories .select-holder');
    $(".form-item-blog-year select#archives-field").wrap("<div class='select-holder'></div>");
    $('.form-item-blog-year span.jcf-select').appendTo('.form-item-blog-year .select-holder');
    $(".form-item-blog-categories select#categories-field").removeAttr('style');
    $(".form-item-blog-year select#archives-field").removeAttr('style');

    $('.form-item-blog-categories').addClass('form-group');
    $('.form-item-blog-year').addClass('form-group');

    $('.form-item-blog-categories select#categories-field').on('change',function(event){
      category = $('.form-item-blog-categories select#categories-field option:selected').val();
      window.location = '/'+ category;
    });

    var month_val = 'Jan';
    var standard = new Array();
    standard[Drupal.t('Jan')] = '01';
    standard[Drupal.t('Feb')] = '02';
    standard[Drupal.t('Mar')] = '03';
    standard[Drupal.t('Apr')] = '04';
    standard[Drupal.t('May')] = '05';
    standard[Drupal.t('Jun')] = '06';
    standard[Drupal.t('Jul')] = '07';
    standard[Drupal.t('Aug')] = '08';
    standard[Drupal.t('Sep')] = '09';
    standard[Drupal.t('Oct')] = '10';
    standard[Drupal.t('Nov')] = '11';
    standard[Drupal.t('Dec')] = '12';
    $('.form-item-blog-year select#archives-field').on('change',function() {
      year = $('.form-item-blog-year select#archives-field option:selected').text();
      path = window.location.pathname;
      result = path.split('/');
      if(result[3] == '' || result[3] == 'archive' || typeof result[3] === "undefined" || result[3] == 'en' || result[3] == 'ar') {
        result[3] = 'babyshop';
      }
      $.ajax({
        url: 'https://://' + window.location.hostname + '/' + country + '/' + country_lang + '/enablemonths/' + result[3] + '/' + year,
      })
      .done(function(data) {
        console.log(data);
        $('input[name="blog_month"]').each(function(){
          $(this).attr("disabled",true);
          $(this).removeAttr("checked");
          $(this).closest('span').addClass('jcf-disabled');
          $(this).closest('div').addClass('form-disabled');
          $(this).closest('div').find('label').removeClass('jcf-label-active');
          if (data.indexOf($(this).val()) >= 0) {
            month_val = $(this).val();
            $(this).attr("disabled",false);
            $(this).closest('span').removeClass('jcf-disabled');
            $(this).closest('div').removeClass('form-disabled');
          }
        })
        year = $('.form-item-blog-year select#archives-field option:selected').text();
        month = $.trim($('input[name="blog_month"]:checked').val());
        window.location = '/' + country + '/' + country_lang + '/archive/' + year + standard[month_val];
      })
    });


    $('input[name="blog_month"]').on('change',function(){
      year = $('.form-item-blog-year select#archives-field option:selected').text();
      month = $.trim($('input[name="blog_month"]:checked').val());
      window.location = '/' + country + '/' + country_lang + '/archive/' + year + standard[month];
    });
    $("#newsLetterForm").validate({
      submitHandler: function(form) {
        var email= $('#newsLetterForm input[type="email"]').val();
        var base_path = window.location.protocol + "//" + window.location.host + "/";
        $.ajax({
          url: base_path + Drupal.settings.basePath + '/subscribe/'+email,
          data: {'email':email},
          success: function(data) {
            if(data == "subscribed_new" || data == "update_subscription") {
              $('#newsLetterForm input').val('');
              $("#newsLetterForm label").closest("label").text("Thanks for signing up to our newsletter.");
              $(".parsley-error-text").hide();
            }
            else if(data == "alreadysubscribed") {
              $('#newsLetterForm input').val('');
              $("#newsLetterForm label").closest("label").text("You have already subscribed.");
              $(".parsley-error-text").hide();
              return false;
            }
            else if(data == "fail") {
              $('#newsLetterForm input').val('');
              $("#newsLetterForm label").closest("label").text("Please check your email address.");
              $(".parsley-error-text").hide();
              return false;
            }
            else 
            {
              return false;
            }
          }
        });
      }
    });

    function valid_email(elem) {
      var re = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
      if (!elem.match(re)) 
        return false;
      else 
        return true;
    }

    $('.select-country').on('click', function() {
      if( $(this).hasClass('active')) {
        $('.hidden').removeAttr('style');
        $(this).removeClass('active');
      }
      else {
        $(this).addClass('active');
        $('.hidden').attr('style','display:block !important');
      }
      if(country_lang == 'en' && country == 'sa') {
        $('ul.country-dropdown').html ('<a href="https://://blog.babyshopstores.com/ae/en"><li class="country-dropdown-li" value="AE"><img class="country-dropdown-logo" src="//i1.lmsin.net/website_images/icons/common/u-a-e@2x.png">United Arab Emirates</li></a>');
		$('ul.country-dropdown').append ('<a href="https://://blog.mothercarestores.com/bh/en"><li class="country-dropdown-li" value="AE"><img class="country-dropdown-logo" src="//i1.lmsin.net/website_images/icons/common/bah.svg">Bahrain</li></a>');
	 }
      else if(country_lang == 'ar' && country == 'sa') {
        $('ul.country-dropdown').html ('<a href="https://://blog.babyshopstores.com/ae/ar"><li class="country-dropdown-li" value="AE"><img class="country-dropdown-logo" src="//i1.lmsin.net/website_images/icons/common/u-a-e@2x.png">الإمارات العربية المتحدة</li></a>');
		$('ul.country-dropdown').append ('<a href="https://://blog.mothercarestores.com/bh/ar"><li class="country-dropdown-li" value="BH"><img class="country-dropdown-logo" src="//i1.lmsin.net/website_images/icons/common/bah.svg">البحرين</li></a>');
	  }
      else if(country_lang == 'en' && country == 'ae') {
        $('ul.country-dropdown').html ('<a href="https://://blog.babyshopstores.com/sa/en"><li class="country-dropdown-li" value="SA"><img class="country-dropdown-logo" src="//i1.lmsin.net/website_images/icons/common/k-s-a@2x.png">Saudi Arabia</li></a>');
		$('ul.country-dropdown').append ('<a href="https://://blog.mothercarestores.com/bh/en"><li class="country-dropdown-li" value="SA"><img class="country-dropdown-logo" src="//i1.lmsin.net/website_images/icons/common/bah.svg">Bahrain</li></a>');
      }
      else if(country_lang == 'ar' && country == 'ae') {
		$('ul.country-dropdown').html ('<a href="https://://blog.babyshopstores.com/sa/ar"><li class="country-dropdown-li" value="SA"><img class="country-dropdown-logo" src="//i1.lmsin.net/website_images/icons/common/k-s-a@2x.png">المملكة العربية السعودية</li></a>');
		$('ul.country-dropdown').append ('<a href="https://://blog.mothercarestores.com/bh/ar"><li class="country-dropdown-li" value="BH"><img class="country-dropdown-logo" src="//i1.lmsin.net/website_images/icons/common/bah.svg">البحرين</li></a>');
        
      }else if(country_lang == 'en' && country == 'bh') {
                   
        $('ul.country-dropdown').html ('<a href="https://://blog.mothercarestores.com/bh/en"><li class="country-dropdown-li" value="SA"><img class="country-dropdown-logo" src="//i1.lmsin.net/website_images/icons/common/bah.svg">Bahrain</li></a>');
        
        $('ul.country-dropdown').append ('<a href="https://://blog.mothercarestores.com/bh/en"><li class="country-dropdown-li" value="AE"><img class="country-dropdown-logo" src="//i1.lmsin.net/website_images/icons/common/bah.svg">Bahrain</li></a>');
      }else if(country_lang == 'ar' && country == 'bh') {
        
        $('ul.country-dropdown').html ('<a href="https://://blog.mothercarestores.com/bh/ar"><li class="country-dropdown-li" value="BH"><img class="country-dropdown-logo" src="//i1.lmsin.net/website_images/icons/common/ksa.svg">البحرين</li></a>');

        $('ul.country-dropdown').append ('<a href="https://://blog.mothercarestores.com/bh/ar"><li class="country-dropdown-li" value="BH"><img class="country-dropdown-logo" src="//i1.lmsin.net/website_images/icons/common/uae.svg">البحرين</li></a>');
      } 
    });
  })
})(jQuery);
