(function($) {
	$(document).ready(function(){
		initCustomSlideselect();
		function initCustomSlideselect() {
			// add slide effect for selects
			$('select').each(function() {
				var select = $(this);
				var jcfAPI = select.data('jcf-instance');
			// destroy custom selects
			if (jcfAPI) jcfAPI.destroy();
			var selectClasses = select.attr('class').split(' ');
			var animSpeed = 400;
			var activeClass = 'jcf-selected';
			var fakeArea = $('<span class="jcf-select"><span class="jcf-select-text"></span><span class="jcf-select-opener"></span></span>');
			var fakeDrop = $('<div class="jcf-select-drop slide-drop"><div class="jcf-select-drop-content"><span class="jcf-list"><span class="jcf-list-content"></span></span></div></div>');
			var fakeSlide = fakeDrop.find('.jcf-list').hide();
			var fakeList = $('<ul/>');
			var valueCell = fakeArea.find('.jcf-select-text');
			var openedState = false;
			// set custom classes
			if (selectClasses) {
				$.each(selectClasses, function(index, className) {
					fakeArea.addClass('jcf-select-' + className);
				});
			}
			// refresh value for fake element
			var refreshValue = function(item, noChange) {
				fakeList.find('.' + activeClass).removeClass(activeClass);
				item.addClass(activeClass);
				valueCell.html(item.text());
				select[0].selectedIndex = item.data('index');
				if (!noChange) {
					select.trigger('change');
					var noChange = true;
				}
				if (item.data('image')) {
					var newImage = new Image();
					newImage.src = item.data('image');
					$(newImage).prependTo(valueCell);
				}
			};
			// toggle active state
			var toggleState = function() {
				if (openedState) {
					openedState = false;
					fakeSlide.stop(true, false).slideUp(animSpeed);
				} else {
					openedState = true;
					fakeSlide.stop(true, false).slideDown(animSpeed);
				}
			};
			// close slide on click outside
			var outsideClickHandler = function(e) {
				var target = $(e.target);
				if (!target.is(fakeArea) && !target.closest(fakeArea).length) {
					openedState = false;
					fakeSlide.stop(true, false).slideUp(animSpeed);
				}
			};
			
			// build fake select
			select.addClass('jcf-hidden').children().each(function(i) {
				var option = $(this);
				var itemHolder = $('<li/>');
				var item = $('<span class="jcf-option"/>');
				var imageSrc = option.attr('data-image');
				var value = option.attr('value');
				if (value) {
					item.attr('data-value', value);
				}
				item.text(option.text()).attr('data-index', i);
			// insert image in drop
			if (imageSrc) {
				item.attr('data-image', imageSrc);
				var newImage = new Image();
				newImage.src = imageSrc;
				$(newImage).prependTo(item);
			}
			itemHolder.append(item);
			fakeList.append(itemHolder);
			// fake item click handler
			item.on('click', function() {
				refreshValue(item);
			});
			// set new value in native select
			var noChange = true;
			if (select.prop('selectedIndex') === i) refreshValue(item,noChange);
				noChange = false;
			});
			// refresh handler for select (for outside events)
			select.on('refresh', function() {
				var value = select.val();
				item = fakeList.find('[data-value="' + value + '"]');
				refreshValue(item, true);
			});
			// append new structure
			fakeDrop.appendTo(fakeArea).find('.jcf-list-content').append(fakeList);
			fakeArea.insertAfter(select).on('click', toggleState);
			// outside click handler
			$(document).on('click touchstart', outsideClickHandler);
			// window resize handler
			$(window).on('scroll resize orientationchange', function() {
				if (openedState) {
					openedState = false;
					fakeSlide.stop(true, false).slideUp(animSpeed);
				}
			});
			});
		}
	});
})(jQuery);