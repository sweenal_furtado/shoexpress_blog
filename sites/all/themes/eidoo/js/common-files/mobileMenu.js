define(['jquery'], function(jQuery) {
	var initMobileMenu = function() {
		var hoverClass = 'hover';
		var hasDropClass = 'has-drop';
		var activeClass = 'active-item';
		var activeMenuClass = 'menu-visible';
		var activeSubClass = 'sub-visible';
		var activeDropClass = 'drop-visible';
		var win = jQuery(window);
		// mobile menu for .header
		jQuery('.header, #main-part, .side-nav').each(function() {
			var holder = jQuery(this);
			var slider = holder.find('.nav-wrap');
			var menu = holder.find('.main-menu');
			var items = holder.find('.first-menu > li').has('.drop');
			var subItems = holder.find('.column > li').has('ul');
			var dropBg = jQuery('<div class="main-menu-drop-bg" />');
			var dropBgHeight = 0;
			menu.append(dropBg);
			// resize handler
			win.on('resize orientationchange', function() {
				// clear height
				items.find('.drop').css({
					height: ''
				});
				dropBg.css({
					height: ''
				});
				slider.css({
					height: ''
				});
				var activeItem = holder.find('.active-item');
				var activeSubItem = activeItem.find('.active-item');
				// check for mobile window width
				if (win.width() <= 767) {
					// set height
					if (activeItem.length) {
						slider.css({
							height: activeItem.find('.drop').outerHeight()
						});
					}
					if (activeSubItem.length) {
						slider.css({
							height: activeItem.find('.hold ul').outerHeight()
						});
					}
				}
			});
			// function for drops
			items.each(function() {
				var item = jQuery(this).addClass(hasDropClass);
				var drop = item.children('.drop');
				var dropContent = drop.children();
				var opener = item.children('a');
				var closer = drop.find('.back a').eq(0);
				// hover handler
				item.on('itemhover', function() {
					// on hover state
					if (win.width() <= 767) {
						drop.css({
							height: ''
						});
						dropBg.css({
							height: ''
						});
						return;
					}
					dropBgHeight = 0;
					if (drop.length) {
						dropBgHeight = dropContent.outerHeight();
					}
					drop.css({
						height: dropBgHeight
					});
					dropBg.css({
						height: dropBgHeight
					});
					items.siblings().find('.drop').css({
						height: dropBgHeight
					});
				}).on('itemleave', function() {
					// on leave state
					if (win.width() <= 767) {
						drop.css({
							height: ''
						});
						dropBg.css({
							height: ''
						});
						return;
					}
					if (!item.hasClass(hoverClass) && !item.find('.' + hoverClass).length) {
						drop.css({
							height: 0
						});
						dropBg.css({
							height: 0
						});
					}
				});
				// click handler
				opener.on('click', function(e) {
					if (win.width() > 767) return;
					// enable active state
					items.removeClass(hoverClass);
					e.preventDefault();
					item.addClass(activeClass);
					holder.addClass(activeSubClass);
					slider.css({
						height: dropContent.outerHeight()
					});
				});
				closer.on('click', function(e) {
					if (win.width() > 767) return;
					e.preventDefault();
					// disable active state
					item.removeClass(activeClass);
					holder.removeClass(activeSubClass);
					slider.css({
						height: ''
					});
				});
			});
			// function for sub drops
			subItems.each(function() {
				var item = jQuery(this).addClass(hasDropClass);
				var drop = item.children('.hold').find('> ul');
				var opener = item.children('.hold').find('> strong a');
				var closer = drop.find('> .back a');
				// opener click handler
				opener.on('click', function(e) {
					if (win.width() > 767) return;
					e.preventDefault();
					// enable active state
					item.addClass(activeClass);
					holder.addClass(activeDropClass);
					slider.css({
						height: drop.outerHeight()
					});
				});
				// closer click handler
				closer.on('click', function(e) {
					if (win.width() > 767) return;
					e.preventDefault();
					// disable active state
					item.removeClass(activeClass);
					holder.removeClass(activeDropClass);
					slider.css({
						height: item.closest('.drop').outerHeight()
					});
				});
			});
			// click handlers
			holder.on('click', '.btn-menu, .btn-close', function(e) {
				e.preventDefault();
				// toggle active class
				holder.toggleClass(activeMenuClass);
				if (!holder.hasClass(activeMenuClass)) {
					holder.removeClass(activeDropClass).removeClass(activeSubClass);
					items.removeClass(activeClass);
					subItems.removeClass(activeClass);
					slider.css({
						height: ''
					});
				}
			});
		});
	};
	return initMobileMenu;
});
