define(['jquery', 'validateCreditCard', 'credit'], function(jQuery, validateCreditCard, credit) {
	var initValidation = function() {
		var showClass = 'show';
		var errorClass = 'error';
		var successClass = 'success';
		var regEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var regPhone = /^[0-9]+$/;

		jQuery('form.validate-form').each(function() {
			var form = jQuery(this).attr('novalidate', 'novalidate');
			var successFlag = true;
			var messageBox = form.closest('.modal').find('.error-message');
			var inputs = form.find('input, textarea, select');

			// form validation function
			function validateForm(e) {
				successFlag = true;
				if (messageBox.length) {
					messageBox.removeClass(showClass);
				}

				inputs.each(checkField);

				if (!successFlag) {
					e.preventDefault();
					if (messageBox.length) {
						messageBox.addClass(showClass);
					}
				}
			}

			// check field
			function checkField(i, obj) {
				var currentObject = jQuery(obj);
				var currentParent = currentObject.closest('.form-group');

				// not empty fields
				if (currentObject.hasClass('required')) {
					setState(currentParent, currentObject, !currentObject.val().length || currentObject.val() === currentObject.prop('defaultValue'));
				}
				// correct email fields
				if (currentObject.hasClass('required-email')) {
					setState(currentParent, currentObject, !regEmail.test(currentObject.val()) || currentObject.val() === currentObject.prop('defaultValue'));
				}
				// correct number fields
				if (currentObject.hasClass('required-number')) {
					setState(currentParent, currentObject, !regPhone.test(currentObject.val()));
				}
				// something selected
				if (currentObject.hasClass('required-select')) {
					setState(currentParent, currentObject, currentObject.get(0).selectedIndex === 0);
				}
			}

			// set state
			function setState(hold, field, error) {
				hold.removeClass(errorClass).removeClass(successClass);
				if (error) {
					hold.addClass(errorClass);
					field.one('focus', function() {
						hold.removeClass(errorClass).removeClass(successClass);
					});
					successFlag = false;
				} else {
					hold.addClass(successClass);
				}
			}

			// form event handlers
			form.submit(validateForm);
		});
		jQuery('.input-card').each(function() {
			var holder = jQuery(this);
			var field = holder.find('input:text');
			var fieldHolder = holder.closest('.form-group');
			var nextField = fieldHolder.nextAll().has('input').eq(0).find('input');
			var hiddenClass = 'hidden-input';
			var refreshClasses = function(name) {
				var classes = holder.attr('class').split(' ');
				for (var i = 0; i < classes.length; i++) {
					var className = classes[i];
					if (className.indexOf('card-') !== -1) {
						holder.removeClass(className);
					}
				}
				holder.addClass('card-' + name);
			};
			field.validateCreditCard(function(result) {
				var value = field.val();
				fieldHolder.removeClass(errorClass);
				if (value.length && !regPhone.test(value)) {
					fieldHolder.addClass(errorClass);
				}
				if (value.length === 16 && !result.card_type) {
					fieldHolder.addClass(errorClass);
				}
				if (result.card_type && result.length_valid) {
					refreshClasses(result.card_type.name);
					nextField.trigger('focus');
				}
			});
			field.addClass(hiddenClass).credit();
			holder.find('.credit-input').addClass('form-control');
		});
	};
	return initValidation;
});
