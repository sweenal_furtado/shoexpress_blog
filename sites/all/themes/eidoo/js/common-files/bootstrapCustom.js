define(['jquery', 'bootstrap'], function(jQuery) {
	var initBootstrap = function() {
		// run BT tooltips
		jQuery('[data-toggle="tooltip"]').tooltip();
		// hold opened state for modal BT
		jQuery(document).on('hidden.bs.modal', function (event) {
			jQuery('.modal').each(function() {
				if (jQuery(this).css('visibility') === 'visible') {
					jQuery('body').addClass('modal-open');
				}
			});
		});
		// add flip effect for modal BT
		jQuery('.modal-content.flip-area').each(function() {
			var flipClass = 'flip';
			var animClass = 'animation';
			var area = jQuery(this);
			var holder = area.closest('.modal');
			var areaId = holder.attr('id');
			var openers = jQuery('a[data-target="#' + areaId + '"]');
			var links = area.find('.modal-opener');
			// click handler for inside openers
			links.on('click', function(e) {
				e.preventDefault();
				var link = jQuery(this);
				if (area.hasClass(animClass)) {
					return;
				}
				area.addClass(animClass);
				if (link.hasClass('flip-enable')) {
					area.addClass(flipClass);
				} else {
					area.removeClass(flipClass);
				}
				// after animation remove animation class
				setTimeout(function() {
					area.removeClass(animClass);
				}, 1000);
				refreshHeight();
			});
			// click handler for outside openers
			openers.on('click', function() {
				var link = jQuery(this);
				if (link.hasClass('flip-enable')) {
					area.addClass(flipClass);
				} else {
					area.removeClass(flipClass);
				}
				refreshHeight();
			});
			// refresh height for area
			function refreshHeight() {
				if (area.hasClass(flipClass)) {
					area.css({
						height: area.find('.card-back-frame').outerHeight() + 2
					});
				} else {
					area.css({
						height: area.find('.card-frame').outerHeight() + 2
					});
				}
			}
			// resize handler
			jQuery(window).on('resize orientationchange', function() {
				jQuery('.modal').each(function() {
					if (jQuery(this).css('visibility') === 'visible') {
						refreshHeight();
					}
				});
			});
		});
	};
	return initBootstrap;
});
