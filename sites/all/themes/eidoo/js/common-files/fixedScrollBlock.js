define(['jquery'], function(jQuery) {
	/*
	 * FixedScrollBlock
	 */
	;(function($, window) {
		'use strict';
		var isMobileDevice = ('ontouchstart' in window) ||
							(window.DocumentTouch && document instanceof DocumentTouch) ||
							/Windows Phone/.test(navigator.userAgent);

		function FixedScrollBlock(options) {
			this.options = $.extend({
				fixedActiveClass: 'fixed-position',
				slideBlock: '[data-scroll-block]',
				positionType: 'auto',
				fixedOnlyIfFits: true,
				container: null,
				animDelay: 100,
				animSpeed: 200,
				extraBottom: 0,
				extraTop: 0
			}, options);
			this.initStructure();
			this.attachEvents();
		}
		FixedScrollBlock.prototype = {
			initStructure: function() {
				// find elements
				this.win = $(window);
				this.container = $(this.options.container);
				this.slideBlock = this.container.find(this.options.slideBlock).addClass('fixed-ready');
				this.fakeBlock = $(this.options.fakeBlock).insertAfter(this.slideBlock);

				// detect method
				if(this.options.positionType === 'auto') {
					this.options.positionType = isMobileDevice ? 'absolute' : 'fixed';
				}
			},
			attachEvents: function() {
				var self = this;

				// bind events
				this.onResize = function() {
					self.resizeHandler();
				};
				this.onScroll = function() {
					self.scrollHandler();
				};

				// handle events
				this.win.on({
					load: this.onResize,
					resize: this.onResize,
					orientationchange: this.onResize,
					scroll: this.onScroll
				});

				// initial handler call
				this.resizeHandler();
			},
			recalculateOffsets: function() {
				var defaultOffset = this.fakeBlock.offset(),
					defaultPosition = this.fakeBlock.position(),
					holderOffset = this.container.offset(),
					windowSize = this.win.height();

				this.data = {
					windowHeight: this.win.height(),
					windowWidth: this.win.width(),

					blockPositionLeft: defaultPosition.left,
					blockPositionTop: defaultPosition.top,

					blockOffsetLeft: defaultOffset.left,
					blockOffsetTop: defaultOffset.top,
					blockHeight: this.fakeBlock.innerHeight(),

					holderOffsetLeft: holderOffset.left,
					holderOffsetTop: holderOffset.top,
					holderHeight: this.container.innerHeight()
				};
			},
			isVisible: function() {
				return this.fakeBlock.prop('offsetHeight');
			},
			fitsInViewport: function() {
				if(this.options.fixedOnlyIfFits && this.data) {
					return this.data.blockHeight + this.options.extraTop <= this.data.windowHeight;
				} else {
					return true;
				}
			},
			resizeHandler: function() {
				this.slideBlock.css({
					width: this.fakeBlock.innerWidth()
				});
				this.fakeBlock.css({
					height: this.slideBlock.outerHeight(true)
				});
				if(this.isVisible()) {
					FixedScrollBlock.stickyMethods[this.options.positionType].onResize.apply(this, arguments);
					this.scrollHandler();
				}
			},
			scrollHandler: function() {
				if(this.isVisible()) {
					if(!this.data) {
						this.resizeHandler();
						return;
					}
					this.currentScrollTop = this.win.scrollTop();
					this.currentScrollLeft = this.win.scrollLeft();
					FixedScrollBlock.stickyMethods[this.options.positionType].onScroll.apply(this, arguments);
				}
			},
			refresh: function() {
				// refresh dimensions and state if needed
				if(this.data) {
					this.data.holderHeight = this.container.innerHeight();
					this.data.blockHeight = this.slideBlock.innerHeight();
					this.scrollHandler();
				}
			},
			destroy: function() {
				// remove event handlers and styles
				this.fakeBlock.remove();
				this.slideBlock.css({
					position: '',
					top: '',
					left: ''
				}).removeClass(this.options.fixedActiveClass).removeClass('fixed-ready');
				this.win.off({
					resize: this.onResize,
					scroll: this.onScroll
				});
			}
		};

		// sticky methods
		FixedScrollBlock.stickyMethods = {
			fixed: {
				onResize: function() {
					this.recalculateOffsets();
				},
				onScroll: function() {
					if(this.fitsInViewport() && this.currentScrollTop + this.options.extraTop > this.data.blockOffsetTop) {
						if(this.currentScrollTop + this.options.extraTop + this.data.blockHeight > this.data.holderOffsetTop + this.data.holderHeight - this.options.extraBottom) {
							this.slideBlock.css({
								position: 'absolute',
								top: this.data.blockPositionTop + this.data.holderHeight - this.data.blockHeight - this.options.extraBottom - (this.data.blockOffsetTop - this.data.holderOffsetTop),
								left: this.data.blockPositionLeft
							});
						} else {
							this.slideBlock.css({
								position: 'fixed',
								top: this.options.extraTop,
								left: this.data.blockOffsetLeft - this.currentScrollLeft
							});
						}
						this.slideBlock.addClass(this.options.fixedActiveClass);
					} else {
						this.slideBlock.removeClass(this.options.fixedActiveClass).css({
							position: '',
							top: '',
							left: ''
						});
					}
				}
			},
			absolute: {
				onResize: function() {
					this.slideBlock.css({
						position: '',
						top: '',
						left: ''
					});
					this.recalculateOffsets();

					this.slideBlock.css({
						position: 'absolute',
						top: this.data.blockPositionTop,
						left: this.data.blockPositionLeft
					});

					this.slideBlock.addClass(this.options.fixedActiveClass);
				},
				onScroll: function() {
					var self = this;
					clearTimeout(this.animTimer);
					this.animTimer = setTimeout(function() {
						var currentScrollTop = self.currentScrollTop + self.options.extraTop,
							initialPosition = self.data.blockPositionTop - (self.data.blockOffsetTop - self.data.holderOffsetTop),
							maxTopPosition =  self.data.holderHeight - self.data.blockHeight - self.options.extraBottom,
							currentTopPosition = initialPosition + Math.min(currentScrollTop - self.data.holderOffsetTop, maxTopPosition),
							calcTopPosition = self.fitsInViewport() && currentScrollTop > self.data.blockOffsetTop ? currentTopPosition : self.data.blockPositionTop;

						self.slideBlock.stop().animate({
							top: calcTopPosition
						}, self.options.animSpeed);
					}, this.options.animDelay);
				}
			}
		};

		// jQuery plugin interface
		$.fn.fixedScrollBlock = function(options) {
			return this.each(function() {
				var params = $.extend({}, options, {container: this}),
					instance = new FixedScrollBlock(params);
				$.data(this, 'FixedScrollBlock', instance);
			});
		};

		// module exports
		window.FixedScrollBlock = FixedScrollBlock;
	}(jQuery, this));

	var initFixedScrollBlock = function() {
		var win = jQuery(window);
		var toggleClass = 'toggle-enabled';
		var visibleClass = 'visible';
		// init fixed state for header
		jQuery('.header').each(function() {
			var header = jQuery(this);
			var btnMenu = header.find('.btn-menu');
			var topBar = header.find('.top-bar');
			var fixedBar = header.find('.main-menu');
			var fixedPosition, hiddenPosition, oldScroll, newScroll;
			var mobileView = btnMenu.is(':visible');
			// scroll page handler
			function scrollHandler() {
				if (mobileView) {
					return;
				}
				newScroll = win.scrollTop();
				fixedBar.css({
					top: - Math.min(newScroll, hiddenPosition + fixedPosition)
				});
				if (win.scrollTop() > fixedPosition + hiddenPosition) {
					header.addClass(toggleClass);
					if (oldScroll < newScroll) {
						header.removeClass(visibleClass);
					} else {
						header.addClass(visibleClass);
					}
				} else {
					header.removeClass(toggleClass).removeClass(visibleClass);
				}
				oldScroll = newScroll;
			}
			// resize page handler
			function resizeHandler() {
				mobileView = btnMenu.is(':visible');
				header.removeAttr('style').removeClass(toggleClass).removeClass(visibleClass);
				hiddenPosition = fixedBar.outerHeight(true);
				fixedPosition = topBar.outerHeight(true);
				scrollHandler();
			}
			// set window handlers
			resizeHandler();
			win.on({
				scroll: scrollHandler,
				load: resizeHandler,
				resize: resizeHandler,
				orientationchange: resizeHandler
			});
		});
		// init fixed scroll block plugin
		jQuery('.checkout-section, .order').fixedScrollBlock({
			slideBlock: '.summary>.holder',
			positionType: 'fixed',
			fakeBlock: '<div class="fake-fixed"></div>',
			extraTop: 75
		});
		jQuery('.help-section #twocolumns').fixedScrollBlock({
			slideBlock: '#faq-menu-list',
			fakeBlock: '<div class="fake-fixed"></div>'
		});
	};
	return initFixedScrollBlock;
});
