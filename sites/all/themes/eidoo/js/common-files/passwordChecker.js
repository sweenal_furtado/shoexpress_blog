define(['jquery'], function(jQuery) {
	var initPasswordChecker = function() {
		// check strong password
		jQuery('.password-row').each(function() {
			var holder = jQuery(this);
			var input = holder.find('input:password');
			var passCells = holder.find('.pass-cell');
			var passClass = 'pass';
			var textCell = holder.find('.text');
			var textValues = textCell.attr('data-text').split(',');
			// refresh strong state
			function refreshState() {
				var value = input.val();
				passCells.removeClass(passClass);
				textCell.text('');
				if (value && value.length) {
					// if password has 4 or less symbols
					if (value.length <= 4) {
						textCell.text(textValues[0]);
						passCells.eq(0).addClass(passClass);
					}
					// if password has 6 or less symbols
					else if (value.length <= 6) {
						textCell.text(textValues[1]);
						for (var i = 0; i < 2; i++) {
							passCells.eq(i).addClass(passClass);
						}
					}
					// if password has 8 or less symbols
					else if (value.length <= 8) {
						textCell.text(textValues[1]);
						for (var i = 0; i < 3; i++) {
							passCells.eq(i).addClass(passClass);
						}
					}
					// if password has 10 or less symbols
					else if (value.length <= 10) {
						textCell.text(textValues[1]);
						for (var i = 0; i < 4; i++) {
							passCells.eq(i).addClass(passClass);
						}
					}
					// if password has more than 10 symbols
					else if (value.length > 10) {
						textCell.text(textValues[2]);
						passCells.addClass(passClass);
					}
				}
			}
			// keyup handler
			input.on('keyup', refreshState);
			refreshState();
		});
	};
	return initPasswordChecker;
});
