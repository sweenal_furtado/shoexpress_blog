define(['jquery', 'intlTelInputUntil', 'intlTelInput'], function(jQuery) {
	var initTelInput = function() {
		// initialization plugin TelInput (for phone inputs)
		jQuery('input[type="tel"]').intlTelInput();
	};
	return initTelInput;
});
