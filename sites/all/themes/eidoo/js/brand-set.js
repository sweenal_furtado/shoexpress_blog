var
  linkTimeoutID,
  clsActive = 'active';

(function($) {
	$(document).ready(function(){

		$('.first-menu').find('li')
	    .on('mouseenter', function () {
	        var $obj = $(this);
	        clearTimeout(linkTimeoutID);
	        linkTimeoutID = setTimeout(function() {
	            $obj.addClass(clsActive);
	        }, 300);
	    })
	    .on('mouseleave', function () {
	        clearTimeout(linkTimeoutID);
	        $(this).removeClass(clsActive);
	    });

	    $('.brand-set').find('.opener').on('click', function() {
	        $(this).parent('.brand-set-block').toggleClass('open');
	    });

        initMobileMenu();

		/*$('#brand-set').on('click',function(e) {
			e.stopPropagation();
			$('ul#brand-set-drop').toggle();

		});
		$(document).on('click', function (e) {
			$('ul#brand-set-drop').hide();

			
		});*/
		
	})
})(jQuery);

function initMobileMenu() {
    var hoverClass = 'hover';
    var hasDropClass = 'has-drop';
    var activeClass = 'active-item';
    var activeMenuClass = 'menu-visible';
    var activeSubClass = 'sub-visible';
    var activeDropClass = 'drop-visible';
    var win = jQuery(window);
    // mobile menu for .header
     jQuery('.header, #main-part').each(function() {
        var holder = jQuery(this),
        slider = holder.find('.nav-wrap'),
        menu = holder.find('.main-menu'),
        items = holder.find('.first-menu > li').has('.drop'),
        subItems = holder.find('.column > li').has('ul');

        // resize handler
        jQuery(window).on('resize', function() {
            slider.css({
                height: ''
            });
            var activeItem = holder.find('.active-item');
            var activeSubItem = activeItem.find('.active-item');
            // check for mobile window width
            if (jQuery(window).width() < 768) {
                // set height
                if (activeItem.length) {
                    slider.css({
                        height: activeItem.find('.drop').outerHeight()
                    });
                }
                if (activeSubItem.length) {
                    slider.css({
                        height: activeItem.find('.hold ul').outerHeight()
                    });
                }
            }
        });
        // function for drops
        items.each(function() {
            var item = jQuery(this).addClass(hasDropClass);
            var drop = item.children('.drop');
            var dropContent = drop.children();
            var opener = item.children('a');
            var closer = drop.find('.back a').eq(0);

            // click handler
            opener.on('click', function(e) {
                if (jQuery(window).width() >= 768) {
                    return;
                }
                // enable active state
                e.preventDefault();
                item.addClass(activeClass);
                holder.addClass(activeSubClass);
                slider.css({
                    height: dropContent.outerHeight()
                });
                jQuery('.main-menu .nav-wrap-outer').animate({ scrollTop: 0 }, 'slow');
            });
            closer.on('click', function(e) {
                if (jQuery(window).width() >= 768) {
                    return;
                }
                e.preventDefault();
                // disable active state
                item.removeClass(activeClass);
                holder.removeClass(activeSubClass);
                slider.css({
                    height: ''
                });
            });
        });
        // function for sub drops
        subItems.each(function() {
            var item = jQuery(this).addClass(hasDropClass);
            var drop = item.children('.hold').find('> ul');
            var opener = item.children('.hold').find('> strong a');
            var closer = drop.find('> .back a');
            // opener click handler
            opener.on('click', function(e) {
                if (jQuery(window).width() >= 768) {
                    return;
                }
                e.preventDefault();
                // enable active state
                item.addClass(activeClass);
                holder.addClass(activeDropClass);
                slider.css({
                    height: drop.outerHeight()
                });
                jQuery('.main-menu .nav-wrap-outer').animate({ scrollTop: 0 }, 'slow');
            });
            // closer click handler
            closer.on('click', function(e) {
                if (jQuery(window).width() >= 768) {
                    return;
                }

                e.preventDefault();
                // disable active state
                item.removeClass(activeClass);
                holder.removeClass(activeDropClass);
                slider.css({
                    height: item.closest('.drop').outerHeight()
                });
            });
        });
        // click handlers
        holder.off('click', '.btn-menu, .btn-close')
        .on('click', '.btn-menu, .btn-close', function(e) {
            e.preventDefault();
            win.scrollTop(0,0);
            // toggle active class
            holder.toggleClass(activeMenuClass);
            if (!holder.hasClass(activeMenuClass)) {
                holder.removeClass(activeDropClass).removeClass(activeSubClass);
                items.removeClass(activeClass);
                subItems.removeClass(activeClass);
                slider.css({
                    height: ''
                });

                jQuery('main, .main-footer').removeClass('menuOpened');
            } else {
                jQuery('main, .main-footer').addClass('menuOpened');

            }
        });
    });
};

