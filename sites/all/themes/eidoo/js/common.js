requirejs.config({
	baseUrl: '',
	shim : {
		'bootstrap':{
			'deps': ['jquery']
		},
		'isotope':{
			'deps': ['jquery', 'imagesloaded', 'bridget']
		}
	},
	paths: {
		'gmaps': 'https://maps.googleapis.com/maps/api/js?v=3.exp',
		'async': 'bower_components/requirejs-plugins/src/async',
		'vendor': 'js/lib',
		'common': 'js/common-files',
		'bower': 'bower_components',
		'hammer': 'js/common-files/hammer',
		'jquery': 'bower_components/jquery1-dist/jquery.min',
		'bootstrap': 'bower_components/bootstrap/dist/js/bootstrap.min',
		'imagesloaded': 'bower_components/imagesloaded/imagesloaded.pkgd.min',
		'isotope': 'bower_components/isotope/dist/isotope.pkgd.min',
		'bridget': 'bower_components/jquery-bridget/jquery.bridget',
		'intlTelInputUntil': 'bower_components/intl-tel-input/lib/libphonenumber/build/utils',
		'intlTelInput': 'bower_components/intl-tel-input/build/js/intlTelInput.min',
		'validateCreditCard': 'js/lib/validateCreditCard',
		'credit': 'js/lib/credit'
	}
});

define(['common/anchors',
		'common/sameHeight',
		'common/bootstrapCustom',
		'common/customForms',
		'common/touchNav',
		'common/openClose',
		'common/mobileMenu',
		'common/telInput',
		'common/validation',
		'common/passwordChecker',
		'common/fixedScrollBlock',
		'common/customSlideSelect',
		'common/picturefill'],
	function(
			initAnchors,
			initSameHeight,
			initBootstrapCustom,
			initCustomForms,
			initTouchNav,
			initOpenClose,
			initMobileMenu,
			initTelInput,
			initValidation,
			initPasswordChecker,
			initFixedScrollBlock,
			initCustomSlideSelect
		) {
		initBootstrapCustom(); // custom script for BT
		initCustomForms(); // custom forms from JCF
		initTouchNav(); // hover helper for devices
		initOpenClose(); // open close with slide/fade effects
		initMobileMenu(); // mobile menu function
		initTelInput(); // phone input function
		initValidation(); // form falidation
		initPasswordChecker(); // check password strong
		initFixedScrollBlock(); // set fixed state on scroll
		initCustomSlideSelect(); // custom select with scroll drop
		picturefill(); // run responsive images
		initAnchors(); // start smooth scroll
		initSameHeight(); // set same height for blocks
	}
);

  $(document).ready(function() {
    //Country Switcher
   $('#dropdown-country > dd > ul > li > a').click(function() {
			var country = $(this).children('span').html().toLowerCase();
      var url = $(location).attr('href');
      redirectUrl = url.replace(Drupal.settings.basePath, "/" + country + "/");
      location.href = redirectUrl;
		});
    });
    