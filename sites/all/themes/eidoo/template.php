<?php

function eidoo_theme($existing, $type, $theme, $path) {
  return array(
    'custom_menu_tree' => array(
      'render element' => 'tree',
    ),
  );
}
function eidoo_preprocess_page(&$vars) {
    global $base_url, $language;
		$arg = arg();
    
		if(($arg[0] == 'taxonomy' && $arg[1] == 'term' && is_numeric($arg[2])) || (!empty($_GET['archive_id']))){
      
			$taxonmy_id = isset($_GET['archive_id']) ? $_GET['archive_id'] : $arg[2];  
			$parent_terms_details = new stdClass();
			$term_detail = taxonomy_term_load($taxonmy_id);
      if($term_detail->vocabulary_machine_name == 'blog_category') {
         drupal_goto($base_url.'/');
      }
			$all_category = isset($term_detail->tid) ? taxonomy_get_children($term_detail->tid) : '';
			$parent_terms_details = $term_detail;
			if(empty($all_category)){
			  $parent_term = isset($term_detail->tid) ? taxonomy_get_parents($term_detail->tid) : '';
				if(!empty($parent_term)){
					reset($parent_term); // reset the array index
					$parent_tid = key($parent_term); // get the key of array
					$all_category = taxonomy_get_children($parent_tid);
					$parent_terms_details = current($parent_term); // get the key of array
				}

			}
      
      // $vars['brand_nav'] = isset($parent_terms_details->field_brand_header[LANGUAGE_NONE][0]['value']) ? $parent_terms_details->field_brand_header[LANGUAGE_NONE][0]['value'] : "";
		}
    
    if (!empty($vars['node'])) {
        if ($vars['node']->type == 'blog') {
            drupal_goto($base_url.'/');
        }
    }
    if($arg[0] == 'blog') {
      drupal_goto($base_url.'/');
    }
}

function eidoo_preprocess_node(&$variables) {
  global $base_url;
  
  $node = $variables['node'];
  if ($node->type == 'blog_ecommerce') {
    $variables['submitted'] = t('Posted on !datetime', array('!datetime' => format_date($node->created, 'custom', 'd M Y')));
    $variables['submitted_datetime'] = format_date($node->created, 'custom', 'Y-m-d');
  }
 
}

function eidoo_preprocess_block(&$variables) {
  if ($variables['block']->module == 'system') {
    $variables['classes_array'][] = 'blogs';
  }
}
function eidoo_menu_tree($variables) {
  // print 'aaaa<pre>';print_r($variables);print '</pre>';
  return '<ul class="list-unstyled">' . $variables['tree'] . '</ul>';
}

function eidoo_preprocess_custom_menu_tree(&$variables) {
  $variables['tree'] = $variables['tree']['#children'];
}

function eidoo_custom_menu_tree($variables) {
   //print 'bbb<pre>';print_r($variables);print '</pre>';exit();
  return '<ul class="row list-unstyled">' . $variables['tree'] . '</ul>';
}

/**
 * Render menus in ul li with title
 */
function render_menu($menu_name) {
  $footer_menu_tree = menu_tree_all_data($menu_name); 
  $footer_menu_expanded = menu_tree_output($footer_menu_tree);
  $footer_menu_expanded['#theme_wrappers'] = array('custom_menu_tree');

  foreach ($footer_menu_expanded as $key => $menu_item) {
    if (!empty($footer_menu_expanded[$key]['#below'])) {
      $footer_menu_expanded[$key]['#attributes']['class'][0] = 'col-sm-2';
      foreach ($footer_menu_expanded[$key]['#below'] as $sub_key => $sub_menu_item) {
        //print '<pre>'; print_r($footer_menu_expanded[$key]['#below'][$sub_key]['#attributes']); print '</pre>';
        if (isset($footer_menu_expanded[$key]['#below'][$sub_key]['#attributes']['class'])) {
          unset($footer_menu_expanded[$key]['#below'][$sub_key]['#attributes']['class']);
        } 
      }
    }
  }

  return $footer_menu_expanded;
}
function eidoo_preprocess_html(&$variables) {
  global $language;
  $variables['classes_array'][] = "et-{$language->language}";
  $variables['classes_array'][] = "country-{$_SESSION['location']}";
}
